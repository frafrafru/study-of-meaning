Alfons Petzold

Der Totschläger

»Hund, elendiger Hund!«

Und dann kam noch ein furchtbarer Fluch durch die niedere Stube geprallt. Der
fiel aber aus keinem wutvergeiferten Mund, sondern aus einer wie zu einem Stein
zornverkrampften Faust und war ein schrecklicher Hieb, der einen blonden,
eisenharten Schädel wie eine leere Zigarrenkiste einschlug.

Als der kräftige junge Männerkörper auf die öldurchtränkten, trittezerkerbten
Steinfliesen des Werkgasthauses, des ehemaligen alten Maschinenhauses,
hinschlug, schwerplumpsig wie ein voller Kartoffelsack, kam es Franz
Scheiblechner, dem Faustschwinger, zum aufrüttelnden Bewußtsein, daß er einen
Menschen totgeschlagen, also ein Mörder geworden war.

Eine atemdrosselnde, schwarze Hülle warf sich über sein bisher so frohes,
zufriedenes Arbeiterleben. Einen Augenblick lang fühlte er sich in einem
stählernen Sarg liegen, an dessen Deckel sich seine lebenshungrige Jugend
vergeblich wundstieß.

Dann weitete sich vor seinen entsetzten Augen die enge, tabakqualmerfüllte
Wirtsstube zu einem mächtigen Raum, der erfüllt war von einer Unzahl Menschen,
die alle Blicke voll Abscheu und Haß für ihn hauen, mit den Fingern auf ihn
wiesen und anklagend ihn anheulten.

»Mörder!«

So litt er schon jetzt zwiefachen Tod: den der Gerechtigkeit und den der
Schande für seine Tat.

Und es kam noch der Tod der Rache dazu.

Denn es schrumpften auf einmal all die vielen Menschen vor ihm zu einer
knöchernen Faust zusammen, die spannte sich um seinen Hals, und er hörte die
Stimme des von ihm Erschlagenen im Weltall dröhnen: »Mörder!«

Eine klagende Frauenstimme riß ihn aus seiner ihn hinrichtenden Betäubung.

»Franz, Franz! Heiliger Gott, was hast denn getan? Schau nicht so wild, ich
bin's ja, dein Annerl !«

Seine Braut war es, die ihn ins Gesicht klagte. Ihre ihn aufrüttelnden Worte
voll Herzweh und fragender wie auch schon wissender Verzweiflung rissen ihn
unbarmherzig über den rotglühenden Rost des Geschehenen.

Und er wachte auf zu tierischer Selbsterhaltung. Aus der brennenden Not seiner
Seele schrie er mit brutaler Härte das weinende Mädchen an.

»Weil der Hundskerl, der Fallot, meine Maschin schimpfiert hat, hab ich ihm
eine feste aufs Dachl geben !«

Darauf ließ er sich ruhig von der erschienenen Polizei abführen.

Der Maschinenwärter Franz Scheiblechner und der Transmissionenaufseher
Ferdinand Gruber, beide in der großen Jutespinnerei angestellt, waren seit Jähr
und Tag gute Freunde gewesen.

In der ewig nebligen, feuchtheißen Dampf- und Ölschwüle des geräumigen
Maschinenhauses hatten sie sich kennen- und schätzengelernt und die Brücke
getreulicher Freundschaft in die fröhlichere Atmosphäre der Gassen,
Vorstadtgärten und Gasthäuser hinübergespannt.

Über diesen wohlgebauten Brückensteg war dann im Laufe der Zeit von den zwei
Freunden schon manche frohe, glückliche Stunde, gar mancher Trost in ihr für
gewöhnlich so graues, mit Verdrießlichkeiten und Enttäuschungen aller Art
vollbeladenes Arbeiterleben getragen worden. Nach Anbruch der abendlichen
Feierzeit und an den freien Sonn- und Feiertagen sah man die beiden ebenso
beisammen wie in der Fabrik, in der sie durch ihre Beschäftigung zumeist immer
in Berührung standen. "Wie Maschine und Transmission, so schienen Scheiblechner
und Gruber untrennbar zu sein.

Keiner besuchte ohne den anderen eine festliche Veranstaltung, machte ohne den
anderen an der Seite einen Ausflug in die Umgebung der Stadt. In jeder
Wählerversammlung, Werkstättenbesprechung und anderen Zusammenkünften der
Arbeiter sah man die beiden Freunde nebeneinander sitzen oder stehen, immer für
die gleiche Meinung sich einsetzend, die in ihnen beinahe die gleichen Worte
bei den Auseinandersetzungen mit den Kameraden weckte. Lachen und Schimpf für
die Erscheinungen ihres Daseins kam aus ihnen in seltener Eintracht nach einem
rätselhaften Gesetz. Sah man an einem regnerischen Sonntagnachmittag den
langen, stricknadeldünnen Gruber Ferdi in der verrauchten Extrastube des
kleinen Vorstadtkaffeehauses die blanken, federweißglänzenden Tarockkarten
schwingen, so konnte man eine todsichere Wette eingehen, daß einer seiner drei
Spielpartner der Maschinenwärter Scheiblechner war.

Der saß dann immer mit seinem klobigen, kräftig untersetzten Knochen- und
Muskelpostament seinem leibschmächtigen Freund gegenüber und lächelte mit
fettglänzendem Ölkannengesicht beruhigend und versöhnlich über die
Aufgeregtheit und den blinden Spieleifer Grubers, während er selbst die
personifizierte Gleichgültigkeit vorstellte. Kein noch so unerwarteter Zufall
konnte ihn in seiner ruhigen Beschaulichkeit stören, und er sah mit dem
gleichen geduldigen Ausdruck des Zuwartens und der Selbstverständlichkeit in
den frischen, wasserblauen Augen auf die sumpfgrüne, abgegriffene Fläche des
breiten Spieltisches nieder, mit dem er gewohnt war, stundenlang in das von ihm
regulierte Kesselfeuer zu schauen. Die schöne Freundschaft zwischen den beiden
an Körper und Seele so ungleichen Menschen bekam mit der Zeit einen solchen
Grad der Innigkeit und des Bewußtseins, eine Einigkeit vorzustellen, daß
Scheiblechner, als er anfing, einem Mädchen aus der Nachbarschaft den Hof zu
machen, vorerst den Gruber ganz ernsthaft fragte, ob er mit diesem Verhältnis
einverstanden sei und dazu raten könne, und erst nach dessen Zustimmung es
wagte, der Weghuber Annerl seine Liebe und ernsten Eheabsichten zu erklären.

Als der bisherige Schlafgenosse des Transmissionenaufsehers auf längere Zeit zu
seinem Regiment als Soldat einrücken mußte, gab der Maschinenwärter sofort
seine hübsche Wohnkammer auf, deren breites Fenster auf licht- und luftreiche
Felder lugte. Er mußte zu seinem einsam gewordenen Freund ziehen, der in einer
halbdunkeln Stube hauste, die dieser mit seinem minderen Gehalt allein schwer
bezahlen konnte. Und Scheiblechner opferte mit Freuden seiner Freundschaft die
bisherigen Schätze seines Proletendaseins:

Licht und werkstättenfremde, reine Luft. Nur in einem waren die Freunde nicht
eines Sinnes, behauptete, jeder starr und steif seine eigene Meinung. Es war
dies ihr Verhältnis zu der Maschine, die sie beide zu bedienen hatten. Der eine
ihren ganzen vielgliedrigen Leib, der andere ihre gewaltigen Füße und Hände,
mit der sie ihre ungeheure Kraft in die entlegensten Räume und Winkel der
Fabrik schleuderte.

Der Maschinenwärter liebte das ihm anvertraute Werk über alles, war stets, auch
in seiner freien Zeit, voll Sorge um die riesigen Räder, den mächtigen
Kesselbauch, die wunderbaren Schrauben, schmirgelte, polierte die Manometer,
Kolben, Griffe, daß sie wie zierliche Sönnchen, Monde und Sternschwänzchen
aufleuchteten; und immer, auch bei der schwersten Arbeit, hing sein Blick voll
Stolz und Liebe an dem ungeheuren Körper des Ungetüms aus Stahl und Kupfer, das
da glühend, brausend, stampfend, zischend, heulend Hunterten Arbeitsmuskeln
tausendfache Kraft und Stärke. Ungebeugtheit vor dem Werke in den Stunden der
Arbeit gab.

Gerade das Gegenteil fühlte der Transmissionenaufseher Gruber. Haß und oftmals
eine unaussprechliche grauenhafte Furcht setzte er gegen die Liebe und das
Vertrauen seines Freundes zur Maschine ein.

Nur mit Widerwillen ging er zur Frühe jedes Tages von neuem an seine verfluchte
Arbeit. Unausgesetzt war er voll des ärgsten Mißtrauens, das ein Schwächerer
gegen eine Ihm feindlich gesinnte Macht empfindet. Umrauschte ihn das ölige
Schleifen der Riemen über die glänzenden Holz-und Stahlwellen, hielt er sich
immer zur Flucht vor einem plötzlichen Überfall bereit.

Wie ein Tierbändiger unter unzuverlässigen, nur mit glühender Eisenstange
niederzuhaltenden Bestien kam er sich vor, solange er seinem Dienst nachgehen,
Antrieb, Übertragungen, Riemenregulatoren nach ihrer Haltbarkeit prüfen, ihre
geheimen Leiden und Wunden suchen und einölen mußte.

Er wurde erst wieder ein Mensch, der sich seines Daseins freuen konnte, wenn
die Dampfsirene Feierabend verkündete und die sausenden Räder und Riemen,
erschlagenen Tigern gleich, mit einem ersterbenden Fauchen in der Dämmerung der
Säle schlaff hinsanken und stumm wurden. Aber ist in Schlaf und Träume hinein
verfolgten ihn die fettigen Polypenarme der stählernen Ungeheuer in der fernen
Fabrik. , Zu Beginn ihrer Freundschaft hatte es Gruber einigemal versucht,
seinem Freund die Menschenfeindschaft der Maschinen zu erklären und in dessen
Herzen den gleichen Haß gegen diese Erfindungen böser Geister und Teufel
großzuziehen, der in ihm sein arges Wesen trieb. Aber seine plumpen
Beschimpfungen und übertriebenen Vorwürfe hatten bei dem sonst so gutmütigen
Maschinenwärter jedesmal Wutausbrüche zur Folge gehabt, und tagelang nachher
war er noch voll Entrüstung gewesen über seines Freundes schandbares Benehmen
gegen die über alles geliebten Maschinen. So verbarg Gruber um des lieben
Friedens willen schlecht und recht seine wahre Gesinnung vor dem Freund. Wenn
nun Scheiblechner vor Kameraden und Bekannten in den höchsten Tönen der
Begeisterung von seinen Dampfkesseln, Akkumulatoren, Motoren sprach, biß sich
Gruber voll unterdrückten Zornes und Schmerzes über die Arglosigkeit des
Anpreisers die Zähne ineinander, um nicht das Gegenteil herau'szubrüllen. Und
insgeheim wuchs in ihm eine große eisige Furcht zur Gewißheit auf, daß da
drinnen in dem roten Maschinenhaus Stahl und Messing, Kupfer und Eisen, Riemen
und Radwerk über sein und seines Freundes Verderben in der Ruhe mancher Nacht
nachsannen.

Der Zeitpunkt der Kesselreinigung war herangekommen. Diese dauerte von Samstag
abends bis Montag früh. Während Gruber dem kontrollierenden Ingenieur das
einwandfreie Funktionieren der Transmissionen und Sicherheitsvorrichtungen
vorführen mußte, saß der Maschinist in dem Fabriksgasthaus, verzehrte sein
Nachtmahl, trank ein Glas Wein und wartete auf seinen Freund, um dann nach
vollständigem Stillstand des Werkes die Feuer zu löschen und die Kessel zum
Einstieg bereitzumachen.

Es waren nur mehr wenige Gäste zugegen, alles Arbeiter der Fabrik, Junggesellen
und einige leichte Brüder, die daheim Weib und Kind auf den Wochenlohn warten
ließen, weil es hier an den Samstagen ein besonders süffiges Bier gab. Der
Wirt, ein ehemaliger Werkmeister, dem eine schwere Walze die Finger der rechten
Hand zermalmt hatte, zählte schon die Tageslosung zusammen, während
Schank-bursche und Kellnerjunge sich schläfrig in den dichten Tabakqualm
hineinlehnten.

Eben wollte sich Scheiblechner noch eine Virginierzigarre bestellen, als die
Tür aufprallte und Gruber wie hereingeschleudert gegen einen Tisch anflog. Er
sah fürchterchterlich aus. Die blaue Zeugbluse hing ihm, mit dem
heruntergerissenen Hemd zu einem Strick verdreht, am Leibe herunter. Seine
nackten Schultern, Brust und Rücken sahen aus, als wären sie durch das
schmutzigste Tropföl gezogen worden. Das Gesicht fahlte leichenhaft unter dem
wirren schwarzen Haarschopf hervor, und die Augen waren die eines wütenden
Hundes, der Wasser sieht. Mit keuchendem Schreien kam es aus seiner stoßenden
Brust: »Jetzt hat s' mich endlich einmal erwischt, das verfluchte Luder. Ich
trete oben auf der Transmissionsbrücken, da ruft mich der Herr Ingenieur; ich
dreh mich um, und schon hat s' mich beim Frack, das elendige eiserne Viech,
dreht mich um die Scheiben, und nur weil der Ingenieur gleich abgestellt hat,
pick ich jetzt nicht als Fettfleck auf der Mauer wie eine Heringsseel! Ich
hab's ja immer g'wußt und g'sagt, daß 's der scheinheilige Hundskrampen auf
mich abg'sehn g'habt hat —«

Erregt fiel ihm da der Maschinenwärter in die Rede: »FerdI, was schimpfst denn
auf die Maschin, du bist ja selbst schuld dran, hätt'st aufg'paßt, hätt s' dich
nicht erwischt!« Der Aufseher war irr vor überstandener Todesangst und Wut; er
spie seinen Freund an: »He, was hast g'sagt, schuld soll ich sein? Ah,
vielleicht hätt ich noch dank schön sagn solln, wenn mich dein Sauwerkel
hing'richt hätt! Mit Dynamit soll s' in die Luft g'sprengt werden, das
vermaledeite Biest!«

»Du, Gruber, halt dich z'rück!«

Mühsam gurgelte es Scheiblechner aus der von Empörung gewürgten Kehle.

»Ich mich z'rückhalten!« brüllte Gruber. »Ich mich z'rückhalten, wo's um mein
Leben gangen ist? Daß ich nicht lach! Und wann jetzt der Direktor vor mir
steht, sag ich ihm dasselbe. Natürlich, du Schlürferl möcht's am liebsten
deiner verdammten Maschin hinten neinkriechen! Aber heut hab ich ihr's g'zeigt,
dem graupign Mistvieh; mit der Spitzhackn hab ich ihr die Schlitzaugn, die zwei
Manometer, vom Schädel g'haut!«

Der Maschinist fühlte Feuer im Gehirn, heiß und rot schießt es ihm in die
Augen. Er taumelte nach vorn und stierte dem Gruber ins Gesicht: »Was hast
getan, was .. .?«

»Deiner eisernen Geliebten den Hirnkasten eing'haut!«

Scheiblechner spürte die Haare auf seinem Kopf brennen, und sein Herz lag in
glühender Lohe. Er sah seine Maschine, sein Werk beschimpft, entehrt. Ein
furchtbares Etwas, gegen das anzukämpfen er nicht mehr die Kraft hatte, ballte
ihm die Faust, riß sie in die Höhe und ließ sie auf die Stirne des Schmähers
fallen. Das Gesicht Grubers höhnte noch einmal fratzenhaft auf, dann verschwand
es mit dem Falle eines schweren Körpers. Als die Faust Scheiblechners pendelnd
zurückfiel, atmete Ferdinand Gruber nicht mehr.

