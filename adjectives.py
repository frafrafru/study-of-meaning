from filereader import load_adjective




class Adjectives():

    import numpy as np
    from sklearn.cluster import KMeans
    from sklearn.cross_decomposition import PLSRegression

    from vectorspace import NounVectorspace, AdjectiveNounVectorspace
    # from filereader import load_adjective

    def __init__(self):

        # Erzeuge Nomen- und AN-Vektorrepräsentationen
        self.nouns = self.NounVectorspace()
        self.adjective_nouns = self.AdjectiveNounVectorspace()

        # initialisiere Menge von String, die Protokoll darüber
        # führt, welche Adjektive hinzugefügt wurden
        self.adjectives = []
        self.adjectives_c = []
        
        # hier werden Tupel der Art (Nomen, Zentroid_zum_Nomen)
        # gespeichert
        self.centroids = []

        # Berechne zunächst alle möglichen Zentroide
        print("Calculating centroids")
        self._calculate_centroids()



    def get_adjective_matrix(self, adj, by_centroid=False):
        """Lädt eine Adjektiv-Matrix. Falls sie nicht existiert,
        dann wird sie erstellt.
        
        :param str adj: Adjektiv, dessen Matrix zurückgegeben werden
                        soll.
        
        :param bool by_centroid: Falls True wird die durch Zentroide
                                 trainierte Matrix zurückgegeben.
                                 
        :return: Adjektiv-Matrix
        :rtype: list"""

        try:
            return load_adjective(adj, centroid=by_centroid)
        except FileNotFoundError:
            self.add_adjective(adj, by_centroid=by_centroid)
            return load_adjective(adj, centroid=by_centroid)


    def add_adjectives(self, adjectives, by_centroid=False):
        """Fügt klassenintern eine Liste von Adjektiven hinzu, sodass
        sie verfügbar gemacht werden.
        
        :param list adjectives: Liste von Adjektiven, die hinzugefügt
                                werden sollen.
        
        :param bool by_centroid: Falls True werden die Matrizen durch Zentroide
                                 trainiert."""

        for adj in adjectives:
            self.add_adjective(adj, by_centroid)


    def add_adjective(self, adj, by_centroid=False):
        """Fügt klassenintern ein Adjektiv hinzu, sodass
        es verfügbar gemacht wird.
        
        :param str adj: Adjektiv, das hinzugefügt werden soll.
        
        :param bool by_centroid: Falls True wird die Matrix durch Zentroide
                                 trainiert."""


        if adj in self.get_added_adjectives(by_centroid):
            return

        # Wenn der Code hierhin kommt, dann muss die Matrix
        # hinzugefügt werden.

        try:
            adj_mat = load_adjective(adj, centroid=by_centroid)
            self.get_added_adjectives(by_centroid).append(adj)

        except FileNotFoundError:
            adj_mat = self._create_adjective_matrix(adj, by_centroid)

            if not by_centroid:
                save_name = "adjectives/{}.csv".format(adj)
                self.adjectives.append(adj)
            else:
                save_name = "adjectives/{}_c.csv".format(adj)
                self.adjectives_c.append(adj)

            print("Saving '{}' matrix...\n{}".format(adj, "-" * 80))
            self.np.savetxt(save_name, adj_mat, delimiter=",")


    def get_added_adjectives(self, by_centroid=False):
        """Gibt die Liste der verfügbar gemachten Adjektive
        zurück.
        
        :param bool by_centroid: Falls True, wird die Liste zurück-
                                 gegeben, die Adjektive enthält, dessen
                                 Matrizen durch Zentroide trainiert wurden.
                                 
        :return: Liste mit verfügbaren Adjektiven
        :rtype: list"""

        if by_centroid:
            return self.adjectives_c
        else:
            return self.adjectives

    

    ############## Zentroide ##############
    def get_centroid(self, noun):
        """Gibt den zentroiden Vektor zu einem Nomen zurück.
        
        :param str noun: Nomen, dessen Zentroid zurückgegeben
                         werden soll.
                         
        :return: Zentroidvektor der zu dem Nomen noun gehört.
        :rtype: list"""

        if not noun in [n for n, _ in self.centroids]:
            self._calculate_centroids(nouns=[noun])

        return [centroid for n, centroid in self.centroids if noun == n][0]
    


    def get_closest_centroids(self, vec, num_closest=10, similiarity_func="cos"):
        """Gibt eine Liste von Nomen zurück, dessen Zentroide am nächsten zu dem
        gegebenen Vektor sind.
        
        :param list vec: Vektor, dessen nächste Zentroide bestimmt werden sollen.
        
        :param int num_closest: Anzahl der nächsten Nomen die zurückgegeben werden
                                sollen.
        
        :param str similarity_func: Wahl der Ähnlichkeitsfunktion, die zur Bestimmung
                                    der 'Nähe' der Vektoren verwendet wird.
                                    
        :return: Liste von nächsten Vektoren"""

        simils = []
        nrm = self.np.linalg.norm(vec)
        
        # iteriere über alle Zentroide und messere deren Ähnlichkeiten zu vec
        for n, centr in self.centroids:

            if similiarity_func in ["cos", "cosine"]:
                    
                inpr = self.np.dot(centr, vec)
                sim = inpr / (self.np.linalg.norm(centr) * nrm)

            else:
                sim = self.np.linalg.norm(vec - centr)

            simils.append((sim, n))
    
        # nach Ähnlichkeit sortieren, um die Vektoren mit den höchsten
        # Ähnlichkeit zurückzugeben
        sort_simils = sorted(simils, key=lambda x:x[0],
                             reverse=similiarity_func in ["cos", "cosine"])
        return [(round(a, 2), b) for a, b in sort_simils[:num_closest]]


    def _calculate_centroids(self, nouns=None):
        """Berechne alle Zentroid-Vektoren zu den angegebenen
        Nomen klassenintern.
        
        :param list nouns: Liste von Nomen. Wenn None, werden
                           alle Nomen, die im Nomenvektorspace
                           vorhanden sind verwendet."""

        if not nouns:
            nouns = self.nouns.get_all_words()

        # Iteriere über alle Nomen und mittle via ANs mit der durch
        # AdjectiveNounVectorspace bereitgestellten Funktion
        for i, noun in enumerate(nouns):
            centroid_n = self.adjective_nouns.get_an_mean_by_noun(noun)

            if not centroid_n == None:
                self.centroids.append((noun, centroid_n))

            if i % 1000 == 0:
                print("Progress: {}%".format(round((i+1) * 100 / len(nouns), 2)))

        out_str = "Finished calculating centroids: {} % of nouns have ANs"
        prc = round(len(self.centroids) / len(nouns) * 100, 2)
        print(out_str.format(prc))



    def _create_adjective_matrix(self, adj, by_centroid=False):
        """Berechne eine Adjektiv-Matrix zu gegebenem Adjektiv.
        
        :param str adj: Das Adjektiv zu dem eine Matrix bestimmt
                        werden soll
                        
        :param bool by_centroid: Wenn True, dann werden die Matrizen
                                 basierend auf den Zentroiden erstellt.
                                 
        :return: Gibt die Adjektiv-Matrix als np.array zurück
        :rtype: np.array"""

        # Alle ANs, die das Adjektiv enthalten
        adj_nouns = self.adjective_nouns.get_ans_by_adj(adj)
        
        # Falls keine Zentroide verwedent werden, dann stammen
        # die Nomen aus dem Nomen-Vektorraum.
        # Im Falle der Zentroide, werden die klassenintern
        # gespeicherten Zentroide verwendet, um Trainigsdaten
        # zu generieren.
        if not by_centroid:    
            all_nouns = self.nouns.get_all_words()
            relev_ans = [(a, n) for a, n in adj_nouns if n in all_nouns]

            X = self.nouns.words2vecs([n for _, n in relev_ans])
        
        else:
            centroid_nouns = [n for n, _ in self.centroids]
            relev_ans = [(a, n) for a, n in adj_nouns if n in centroid_nouns]

            X = [self.get_centroid(n) for _, n in relev_ans]
        
        Y = self.adjective_nouns.words2vecs(relev_ans)

        # Hier wird sichergestellt, dass die Matrizen am Ende
        # quadratisch sind. Bei Zentroiden sind Matrizen auto-
        # matisch immer quadratisch.
        assert(len(X[0]) == len(Y[0]))

        # Das Modell hat die Form: Y = X * A,
        # wobei A die PLS-Regressionsmatrix ist.
        pls = self.PLSRegression(n_components=len(X[0]))
        pls.fit(X, Y)

        # Transponieren bringt das Ergebnis in die Form: B * X = Y,
        # wobei B = A^T. Wertet man die Einheitsmatrix darin aus,
        # erhält man die gewünschte Adjektivmatrix
        ident = self.np.identity(len(X[0]))
        return self.np.transpose(pls.predict(ident))
            


    def find_adjectives_with_most_ans(self, num_freq=20):
        """Findet die Adjektive, die die meisten ANs im
        Vektorraum besitzen.
        
        :param int num_freq: Die Anzahl der Adjektive, die zurück-
                             gegeben werden sollen.

        :return: Gibt eine Liste von Adjektiven zurück.
        :rtype: list"""

        # alle einzigartigen Adjektive, die in ANs vorkommen
        unique_adj_in_ans = set(self.adjective_nouns.get_adjectives_from_ans())

        nums = []

        # Iteriere über alle einzigart. Adjektive und zähle
        # ANs für jedes Adjektiv. Anschließend werden diese
        # sortiert und die mit dem höchsten Wert zurückgegeben.
        for adj in unique_adj_in_ans:
            ans = self.adjective_nouns.get_ans_by_adj(adj)
        
            all_nouns = self.nouns.get_all_words()
            relev_ans = [(a, n) for a, n in ans if n in all_nouns]

            nums.append((adj, len(relev_ans)))

        sorted0 = sorted(nums, key=lambda x: x[1], reverse=True)   # highest first

        return [a for a,_,_ in sorted0[:num_freq]]



    def cluster(self, adjectives=None, index_col=None, num_clusters=None, 
                power=1, by_centroid=False):
        """Bildet Cluster mit den angegebenen Adjektiven. Matrizen 
        werden zeilenweise konkateniert und als Vektoren gruppiert.
        Falls index_col nicht None ist, wird die angegebene Spalte
        verwendet, um die Cluster zu bilden.
        
        :param list adjectives: Wenn nicht None, werden die angege-
                                benen Adjektiv-Matrizen geclustert.
                                Wenn None, werden alle hinzugefügten
                                Adjektive verwendet.

        :param int index_col: Falls None, werden die Matrizen zeilen-
                              weise konkateniert und die resultierenden
                              Vektoren geclustert. Wenn nicht None,
                              wird die angegebene Spalte als Vektor ver-
                              wendet.

        :param int num_clusters: Falls nicht None, werden so viele Cluster
                                 gebildet. Falls None, so werden so viele
                                 Cluster gebildet, wie es Adjektive gibt.

        :param int power: Erhebt die Matrizen zur angegebenen Potenz, bevor
                          die Matrizen zeilenweise konkateniert bzw. 
                          Spalten ausgewählt werden.

        :param bool by_centroid: Falls True, werden die durch Zentroide
                                 erstellte Adjektiv-Matrizen verwendet.

        :return: Gibt ein Wörterbuch zurück, das die Cluster enthält.
        :rtype: dict"""


        # Falls adjectives None ist, werden alle hinzugefügten Adj. verwendet.
        if not adjectives:
            adjectives = self.get_added_adjectives(by_centroid)

        # Erstelle Datensatz der an KMeans übergeben wird
        X = []
        for adj in adjectives:

            product = self.np.linalg.matrix_power(self.get_adjective_matrix(adj, by_centroid), power)

            if not index_col:
                X.append([cell for row in product for cell in row])                  
            else:
                X.append([row[index_col] for row in product])
                

        # Falls keine Anzahl an Clustern angegeben wurde, wird
        # die Anzahl an Adjektiven verwendet.
        if not num_clusters:
            num_clusters = len(adjectives)

        # Hier findet das Clustering statt
        kmeans = self.KMeans(n_clusters=num_clusters, random_state=0).fit(X)
        predictions = kmeans.predict(X)

        # Ordne gefundene Cluster in einem Dict an
        clusters = dict([(cat, []) for cat in predictions])
        for pred, adj in zip(predictions, adjectives):
            clusters[pred].append(adj)

        return clusters



    def cluster_with_nouns(self, adjectives, noun, num_clusters=None, power=1, by_centroid=False):
        """Bildet Cluster mit den angegebenen Adjektiven, indem 
        die Adjektiv-Matrizen mit dem Nomen-Vektor des angegebenen
        Nomens multipliziert werden.
        
        :param list adjectives: Die Bildvektoren unter den Matrizen der
                                angegebenen Adjektive werden geclustert.

        :param int index_col: Spezifiziert das Nomen, dessen Vektor zur
                              Multiplikation mit den Matrizen verwendet
                              werden soll.

        :param int num_clusters: Falls nicht None, werden so viele Cluster
                                 gebildet. Falls None, so werden so viele
                                 Cluster gebildet, wie es Adjektive gibt.

        :param int power: Erhebt die Matrizen zur angegebenen Potenz, bevor
                          die Matrizen zeilenweise konkateniert bzw. 
                          Spalten ausgewählt werden.

        :param bool by_centroid: Falls True, werden die durch Zentroide
                                 erstellte Adjektiv-Matrizen verwendet.

        :return: Gibt ein Wörterbuch zurück, das die Cluster enthält.
        :rtype: dict"""


        # erstelle Datensatz, der an KMeans übergeben wird
        X = []
        for adj in adjectives:

            product = self.np.linalg.matrix_power(self.get_adjective_matrix(adj, by_centroid), power)
            X.append(self.np.dot(product, self.nouns.words2vecs(noun)))

        # Falls keine Anzahl an Clustern angegeben wurde, so
        # wird standardmäßig die Anzahl der Adjektive verwendet.
        if not num_clusters:
            num_clusters = len(adjectives)

        # Hier findet das Clustering statt.
        kmeans = self.KMeans(n_clusters=num_clusters, random_state=0).fit(X)
        predictions = kmeans.predict(X)

        # Ordne Cluster in einem Dict an
        clusters = dict([(cat, []) for cat in predictions])
        for pred, adj in zip(predictions, adjectives):
            # print("Kategorie: {} für Adj. {}".format(pred, adj))
            clusters[pred].append(adj)

        return clusters



    def get_mean_distances_by_power(self, adjs, nouns, powers=[1,2,3,4,5]):
        """Bildet Mittel der Abstände der angegebenen Nomen bzgl.
        des Vektors der einem jeweiligen Adjektiv*Nomen multiplizierten
        Vektor (mit Potenz 1) ist. (Siehe Erklärung des Verfahrens in der
        Dokumentation)
        
        :param list adjs: Die Liste der Adjektive, die überprüft werden.

        :param list nouns: Nomen, dessen Vektoren verwendet werden, um 
                           Abstände zu berechnen, die gemittel werden.

        :param list powers: Die Potenzen, die einzelnd untersucht werden
                            und dessen gemittelte Abstände zurückgegeben
                            werden.

        :return: Gibt eine Liste zurück, die die gemittelten Abstände
                 enthält..
        :rtype: list"""
        

        adj_means = dict()

        # Iteriere über alle Adjektive und erstelle für jedes Adjektiv
        # und jede Potenz einen Mittelwert
        for adj in adjs:

            # Initialisiere die zugehörige Matrix und eine Liste
            # die alle Werte speichert, über die später gemittelt
            # wird.
            mat = self.get_adjective_matrix(adj, by_centroid=True)
            to_means = []

            # Iteriere über alle Nomen, sodass für jedes Nomen ein Wert
            # gesammelt wird (die zum Mittelwert beitragen)
            for noun in nouns:

                # Berechne das Nomen (Zentroid), der der Matrix-Multiplikation
                # am nächsten ist, und dessen Abstand.
                dist, centr = self.get_closest_to_mat_application(adj, noun, 
                                                                  by_centroid=True)[0]

                # Tatsächlicher Vektor obiges Nomens und dessen
                # Norm. (Wird gleich öfter benötigt.)
                centr_vec = self.get_centroid(centr)
                centr_vec_norm = self.np.linalg.norm(centr_vec)

                
                # Iteriere über alle Potenzen und berechne alle Abstände
                # und deren Differenzen zum ersten Abstand "dist"
                distances = []
                for power in powers:
                    
                    # Potenzierung der Matrix und Berechnung des Bildvektors
                    mat_to_power = self.np.linalg.matrix_power(mat, power)
                    img_vec = self.np.dot(mat_to_power, self.get_centroid(noun))
                    
                    # Berechnung der Kosinus-Ähnlichkeit (Hinzufügen in die Liste)
                    inpr = self.np.dot(centr_vec, img_vec)
                    cosim = round(inpr/(centr_vec_norm * self.np.linalg.norm(img_vec)), 2)

                    distances.append(round(dist - cosim, 2))

                to_means.append(distances)

            # Hier werden die jeweiligen Mittelwerte berechnet.
            adj_means[adj] = [round(self.np.mean([val[i] for val in to_means]), 2) 
                              for i in range(len(to_means[0]))]

        return adj_means



    
    def get_det(self, adj, by_centroid=False):
        """Berechnet die Determiniante der Matrix des
        angegebenen Adjektivs.
        
        :param str adj: Adjektiv, dessen Matrix Determinante
                        berechnet wird.
                        
        :param bool by_centroid: Falls True, wird die durch Zentroide
                                 erstellte Adjektiv-Matrix verwendet.
                                 
        :return: Gibt die Determiniante (Integer) zurück.
        :rtype: int"""

        return self.np.lingalg.det(self.get_adjective_matrix(adj, by_centroid))



    def there_and_back(self, adj1, adj2, noun, num_closest=10, 
                       by_centroid=False, power=1):
        """Wendet zuerst die Matrix des Adjektivs adj1 auf den
        zu noun gehörenden Vektor an und anschließend das Inverse
        der zu adj2 gehörenden Matrix. Es werden dann die nächsten
        zu diesem Ergebnis liegenden Vektoren zurückgegeben. (Diese
        Funktion wurde verwendet, um bedeutungsähnliche Adjektive zu
        finden.)
        
        :param str adj1: Adjektiv, dessen Matrix nicht invertiert wird.
        
        :param str adj2: Adjektiv, dessen Matrix invertiert wird.
        
        :param str noun: Nomen, dessen Vektor für die Matrix-Multipli-
                         kation verwendet wird.
                         
        :param int num_closest: Anzahl der nächsten Vektoren, die zurück-
                                gegeben werden.
                                
        :param bool by_centroid: Wenn True, werden die durch Zentroide er-
                                 stellte Matrizen verwendet.
                                 
        :param int power: Erhebt die Matrix zu dieser Potenz, bevor eine
                          Rechnung durchgeführt wird.
                          
        :return: Liste mit den nächsten Vektoren.
        :rtype: list"""


        # Initialisierung der Matrizen
        mat1 = self.get_adjective_matrix(adj1, by_centroid)
        mat2 =self.get_adjective_matrix(adj2, by_centroid)

        # Potenzierung der Matrizen
        mat1 = self.np.linalg.matrix_power(mat1, power)
        mat2 = self.np.linalg.matrix_power(mat2, power)

        # Matrixprodukt (B^{-1} * A)
        prod = self.np.dot(self.np.linalg.inv(mat2), mat1)
        
        # Wenn Zentroide verwendet werden, so wird auch
        # der Zentroid-Nomen-Vektor verwendet.
        # Gib anschließend die nächsten Zentroide bzw.
        # Nomen-Vektoren zurück.
        if by_centroid:
            noun_vec = self.get_centroid(noun)
            noun_mat_app = self.np.dot(prod, noun_vec)

            return self.get_closest_centroids(noun_mat_app, 
                                              num_closest=num_closest)

        else:
            noun_vec = self.nouns.words2vecs(noun)
            return self.nouns.get_closest_to_vector(self.np.dot(prod, noun_vec),
                                                    num_closest=num_closest)



    def get_spectrum(self, adj, noun, by_centroid=False, a=lambda x: x, 
                     num_samples=20):
        """Gibt einen Vektor zurück, der die Skala eines graduierbaren
        Adjektivs entsprechen soll. Das Verfahren hierzu wird in der 
        Dokumentation beschrieben (Kapitel Skalen erzeugen).
        
        :param str adj: Adjektiv, dessen Skala erzeugt wird.
                
        :param str noun: Nomen, dessen Vektor für die Skalenerezeugung
                         verwerdet wird.
                                
        :param bool by_centroid: Wenn True, werden die durch Zentroide er-
                                 stellte Matrizen verwendet.
                                 
        :param int num_samples: Die Anzahl der Trainingsdaten, die zur
                                Erstellung der Skala verwendet wird.

        :param lambda a: monotone Funktion, die zur Erzeugung der Trainings-
                         daten verwendet wird.
                          
        :return: Vektor, der die Skala repräsentiert
        :rtype: np.array"""


        from sklearn.linear_model import LinearRegression

        adj_mat = self.get_adjective_matrix(adj, by_centroid)

        # Zentroide oder nicht?
        if by_centroid:
            noun_vec = self.get_centroid(noun)
        else:
            noun_vec = self.nouns.words2vecs(noun)

        # Erzeuge Datenpaare: <Av, a(1)>, <A^2v, a(2)>, ... 
        X = self.np.array([self.np.dot(self.np.linalg.matrix_power(adj_mat, n+1), noun_vec) for n in range(num_samples)])
        y = self.np.array([a(n+1) for n in range(num_samples)])

        # Hier wird die Skala durch die Trainingsdaten erzeugt
        reg = LinearRegression().fit(X, y)

        # Gib die Koeffizienten als Vektor zurück.
        return reg.coef_



    def get_spectrum_by_given_nouns(self, adj, nouns, by_centroid=False, 
                                    a=lambda x: x, num_samples_per_noun=5):
        """Gibt einen Vektor zurück, der die Skala eines graduierbaren
        Adjektivs entsprechen soll. Funktioniert wie adjectives.get_spectrum
        allerdings werden hier mehrere Nomen zur Erzeugung der Datenpaare
        verwendet. (Siehe ebenfalls Dokumentation für eine Erläuterung des
        Verfahrens)
        
        :param str adj: Adjektiv, dessen Skala erzeugt wird.
                
        :param list nouns: Liste von Nomen, deren Vektoren für die Skalenerezeugung
                           verwerdet werden.
                                
        :param bool by_centroid: Wenn True, werden die durch Zentroide er-
                                 stellte Matrizen verwendet.
                                 
        :param lambda a: monotone Funktion, die zur Erzeugung der Trainings-
                         daten verwendet wird.
                          
        :param int num_samples_per_noun: Die Anzahl der Trainingsdaten, die zur
                                         Erstellung der Skala pro Nomen verwendet
                                         wird.

        :return: Vektor, der die Skala repräsentiert
        :rtype: np.array"""


        # Analog zu obiger Funktion
        from sklearn.linear_model import LinearRegression

        adj_mat = self.get_adjective_matrix(adj, by_centroid)

        X, y = [], []

        for noun in nouns:

            if by_centroid:
                noun_vec = self.get_centroid(noun)
            else:
                noun_vec = self.nouns.words2vecs(noun)

            X += [self.np.dot(self.np.linalg.matrix_power(adj_mat, n+1), noun_vec) for n in range(num_samples_per_noun)]
            y += [a(n+1) for n in range(num_samples_per_noun)]

        X = self.np.array(X)
        y = self.np.array(y)

        reg = LinearRegression().fit(X, y)

        return reg.coef_



    def arrange_on_spectrum(self, adjectives, nouns, by_centroid=True,
                            num_samples_per_noun=5):
        """Berechnet die Skalen der graduierbaren Adjektive auf Basis
        der angegebenen Nomen und ordnet dann alle ANs bestehend aus
        den angegebenen Adjektiven + angegebenen Nomen auf diesen Skalen an.
        
        :param list adjs: Adjektive, auf deren Skalen angeordnet wird.
                
        :param list nouns: Liste von Nomen, deren Vektoren für die Skalen-
                           erezeugung verwerdet werden und die durch Kombi-
                           nation der Adjektive als ANs auf den Skalen an-
                           geordnet werden.
                                
        :param bool by_centroid: Wenn True, werden die durch Zentroide er-
                                 stellte Matrizen verwendet.
                                 
        :param lambda a: monotone Funktion, die zur Erzeugung der Trainings-
                         daten verwendet wird.
                          
        :param int num_samples_per_noun: Die Anzahl der Trainingsdaten, die zur
                                         Erstellung der Skala pro Nomen verwendet
                                         wird.

        :return: Wörterbuch, das Listen mit den Anordnungen enthält.
        :rtype: dict"""


        result = dict()

        # Sammle alle ANs, die relevant sind
        relevant_ans = []
        for adj in adjectives:
            relevant_ans += self.adjective_nouns.get_ans_by_adj(adj)

        # Für jedes Adjektiv wird die Skala erzeugt
        # und anschließend die jeweiligen ANs angeordnet.
        # Die Anordnung wird anhand der Kosinus-Ähnlichkeit
        # durchgeführt.
        for adj in adjectives:

            # Erzeuge Skala des Adjektivs
            spec = self.get_spectrum_by_given_nouns(adj, nouns, by_centroid=by_centroid, num_samples_per_noun=num_samples_per_noun)
            similarities = []

            # Für jedes relevante AN wird die Ähnlichkeit  berechnet.
            for an_vec, an in zip(self.adjective_nouns.words2vecs(relevant_ans), relevant_ans):
                cosim = self.np.dot(spec, an_vec)
                similarities.append((cosim, an))

            # Sortieren und zurückgeben
            sorted_sims = sorted(similarities, key=lambda x:x[0], reverse=True)
            result[adj] = [(b, round(a,2)) for a, b in sorted_sims if b[1] in nouns]

        return result



    
    

    def get_closest_to_mat_application(self, adj, noun, num_closest=10, by_centroid=False,
                                       power=1, closest_centroid=True):
        """Wird nicht verwendet. Berechnet die nächsten Vektoren
        zu dem Bildvektor von noun unter der Matrix adj."""

        mat = self.np.linalg.matrix_power(self.get_adjective_matrix(adj, by_centroid), power)

        if by_centroid:
            noun_vec = self.get_centroid(noun)

            
            if closest_centroid:
                return self.get_closest_centroids(self.np.dot(mat, noun_vec), num_closest=num_closest)
        else:
            noun_vec = self.nouns.words2vecs(noun)
        
        return self.adjective_nouns.get_closest_to_vector(self.np.dot(mat, noun_vec), num_closest=num_closest)



    def chain(self, adj, start_word, by_centroid=False, max_iter=10):
        """Wird nicht verwendet. Bildet eine Kette, indem ein start_word
        (Nomen) unter der Matrix von adj abgebildet wird. Dann wird das
        Inverse der angegebenen Matrix auf den des Bildvektors am
        nächsten liegenden Vektors angewandt und das resultierende
        Ergebnis wieder unter der Matrix von adj abgebildet. Führt
        maximal max_iter Durchläufe aus und hört auf, falls es in
        eine Schleife gerät (d.h. ein Vektor bereits aufgetreten ist)"""


        start_vec = self.nouns.words2vecs(start_word)
        mat_adj = self.get_adjective_matrix(adj, by_centroid)
        mat_adj_inv = self.np.linalg.inv(mat_adj)


        print("Start at (" + adj + ", " + start_word + ")")

        outp_str = start_word


        prev_ns = [start_word]

        for i in range(max_iter):
            # abbilden
            v_an = self.np.dot(mat_adj, start_vec)

            # get closest word to vec
            existing_word_an = self.adjective_nouns.get_closest_to_vector(v_an, num_closest=1)[0]

            # reverse back to noun space
            v_n = np.dot(mat_adj_inv, self.adjective_nouns.words2vecs([existing_word_an])[0] )

            existing_word_n = self.nouns.get_closest_to_vector(v_n, 1)[0]

            # print("Nach {}. Durchlauf: nächster in an: {}, nächster in n: {}".format(i+1, existing_word_an, existing_word_n))
            outp_str += " --[" + adj + "]--> " + str(existing_word_an) + " --[" + adj + "^(-1)]--> " + existing_word_n

            if existing_word_n in prev_ns:
                print(outp_str)
                print("RECURRENT AFTER", i+1, "steps")
                return

            prev_ns.append(existing_word_n)

            start_vec = self.nouns.words2vecs(existing_word_n)

        print(outp_str)








# adjectives with most ANs (top 50)
all_adj = ['groß', 'klein', 'neu', 'alt', 'ganz', 'gut', 'erster', 'letzter', 'schön', 'hoch', 'eigen', 'jung', 'lang', 'einzig', 'weit', 'schwarz', 'weiß', 'gewiß', 'zweiter', 'tief', 'gering', 'schwer', 'stark', 'arm', 'gleich', 'verschieden', 'voll', 'wild', 'kurz', 'besonderer', 'furchtbar', 'dunkel', 'allgemein', 'heilig', 'ungeheu', 'fremd', 'wahr', 'englisch', 'leicht', 'rot', 'früh', 'nächster', 'lieb', 'grün', 'schrecklich', 'fein', 'schlecht', 'mächtig', 'seltsam', 'unglücklich']


adjectives = Adjectives()

adjectives.add_adjectives(all_adj, by_centroid=False)
adjectives.add_adjectives(all_adj, by_centroid=True)



# weniger frequente (top 51-400), nicht gebraucht
# all_adj_ext = ['golden', 'glücklich', 'dick', 'menschlich', 'deutsch', 'breit', 'übrig', 'edel', 'französisch', 'herrlich', 'blau', 'innerer', 'reich', 'still', 'leise', 'kalt', 'geheim', 'gewöhnlich', 'einfach', 'gewaltig', 'eu', 'warm', 'dritter', 'fest', 'einzeln', 'böse', 'halb', 'hell', 'glänzend', 'freundlich', 'frei', 'grau', 'heftig', 'ernst', 'natürlich', 'berühmt', 'ewig', 'streng', 'traurig', 'rechter', 'heiß', 'rein', 'scharf', 'russisch', 'wichtig', 'offen', 'ruhig', 'lebhaft', 'hübsch', 'gefährlich', 'schwach', 'prächtig', 'eisern', 'richtig', 'fern', 'bestimmt', 'falsch', 'sonderbar', 'öffentlich', 'stolz', 'folgend', 'zart', 'politisch', 'schlimm', 'merkwürdig', 'eng', 'le', 'schmal', 'wirklich', 'entsetzlich', 'süß', 'kräftig', 'hart', 'niedrig', 'ähnlich', 'oberer', 'sanft', 'plötzlich', 'königlich', 'frisch', 'unbekannt', 'vornehm', 'gelb', 'bunt', 'persönlich', 'einsam', 'angenehm', 'kühn', 'dicht', 'treu', 'kostbar', 'nah', 'fromm', 'fürchterlich', 'wunderbar', 'lustig', 'klar', 'geistig', 'ehrlich', 'elend', 'weich', 'zahlreich', 'düster', 'europäisch', 'nämlich', 'äußerer', 'äußerst', 'amerikanisch', 'braun', 'sicher', 'leidenschaftlich', 'glühend', 'möglich', 'blutig', 'brennend', 'künftig', 'eigentlich', 'bloß', 'toll', 'dünn', 'genau', 'bekannt', 'laut', 'weiblich', 'fröhlich', 'tüchtig', 'grausam', 'unerwartet', 'rund', 'gewohnt', 'ungewöhnlich', 'heiter', 'tot', 'silbern', 'bedeutend', 'feucht', 'dumm', 'klug', 'echt', 'kühl', 'unangenehm', 'schnell', 'außerordentlich', 'reizend', 'irdisch', 'köstlich', 'feierlich', 'geheimnisvoll', 'moralisch', 'spanisch', 'rauh', 'rasch', 'verzweifelt', 'kaiserlich', 'nackt', 'bitter', 'finster', 'erhaben', 'dumpf', 'günstig', 'stumm', 'heimlich', 'göttlich', 'vierter', 'ausgezeichnet', 'gemeinsam', 'gemein', 'entfernt', 'derartig', 'liebenswürdig', 'riesig', 'trüb', 'völlig', 'grob', 'unendlich', 'schmutzig', 'vollkommen', 'gelegen', 'lebendig', 'römisch', 'stehend', 'gesund', 'doppelt', 'südlich', 'militärisch', 'unterer', 'blaß', 'feindlich', 'jetzig', 'liegend', 'schottisch', 'selig', 'bescheiden', 'zärtlich', 'verloren', 'benachbart', 'brav', 'würdig', 'christlich', 'bürgerlich', 'bleich', 'unheimlich', 'nützlich', 'drohend', 'ehemalig', 'kindlich', 'roh', 'interessant', 'roth', 'trocken', 'himmlisch', 'schmerzlich', 'griechisch', 'tapfer', 'nächtlich', 'hölzern', 'peinlich', 'männlich', 'vortrefflich', 'friedlich', 'flüchtig', 'unbestimmt', 'selten', 'häßlich', 'innig', 'mild', 'nötig', 'freudig', 'wunderlich', 'blühend', 'linker', 'paris', 'gütig', 'leuchtend', 'gelehrt', 'blond', 'jäh', 'geworden', 'ehrwürdig', 'lieblich', 'wacker', 'froh', 'krank', 'übel', 'religiös', 'erregt', 'seiden', 'verlassen', 'ernsthaft', 'bequem', 'nöthig', 'weise', 'beträchtlich', 'jugendlich', 'unschuldig', 'gegenseitig', 'spitz', 'wissenschaftlich', 'erwidert', 'italienisch', 'wachsend', 'vorzüglich', 'verdammt', 'steinern', 'glatt', 'geschlossen', 'lebend', 'töricht', 'spät', 'grenzenlos', 'geschickt', 'aufrichtig', 'abscheulich', 'erstaunlich', 'vernünftig', 'schwierig', 'steil', 'entscheidend', 'unbedeutend', 'vollständig', 'nördlich', 'unruhig', 'mager', 'väterlich', 'herrschend', 'praktisch', 'unnütz', 'kommend', 'naß', 'gräßlich', 'körperlich', 'geliebt', 'rührend', 'heutig', 'modern', 'elegant', 'türkisch', 'eigentümlich', 'feurig', 'teu', 'ängstlich', 'elektrisch', 'müde', 'unerhört', 'flach', 'endlos', 'weltlich', 'hervorragend', 'wütend', 'üppig', 'bevorstehend', 'herzlich', 'anständig', 'rasend', 'reichlich', 'großartig', 'licht', 'vorig', 'schlank', 'blank', 'zerrissen', 'täglich', 'öd', 'häuslich', 'notwendig', 'verhängnisvoll', 'geistlich', 'verborgen', 'stattlich', 'nett', 'regelmäßig', 'unzählig', 'zitternd', 'harmlos', 'nieder', 'kriegerisch', 'ordentlich', 'dürr', 'vollendet', 'stürmisch', 'bewegt', 'unermeßlich', 'philosophisch', 'eigenthümlich', 'verstorben', 'arg', 'beweglich', 'gesellschaftlich', 'unterirdisch', 'fett']

# adjectives.add_adjectives(all_adj_ext, by_centroid=False)
# adjectives.add_adjectives(all_adj_ext, by_centroid=True)




# Die Top 60 Nomen mit ANs
all_nouns = ['Leben', 'Weise', 'Arbeit', 'Licht', 'Frau', 'Stelle', 'Blick', 'Hand', 'Dame', 'Land', 'Gestalt', 'Ton', 'Stunde', 'Lippe', 'Haus', 'Nacht', 'Art', 'Ruhe', 'Meer', 'Miene', 'Gedanke', 'Wort', 'Freude', 'Zeit', 'Liebe', 'Tag', 'Kraft', 'Grund', 'Wesen', 'Stadt', 'Zimmer', 'Herr', 'Jahr', 'Auge', 'Mensch', 'Wasser', 'Teil', 'Platz', 'Haar', 'Stimme', 'Kopf', 'Fuß', 'Freund', 'Gesicht', 'Seele', 'Gefühl', 'Lächeln', 'Kind', 'Herz', 'Leute', 'Bewegung', 'Welt', 'Mädchen', 'Vater', 'Lage', 'Augenblick', 'Ding', 'Sache', 'Zug', 'Mann']




########################################
######### CLUSTER MIT MATRIZEN #########
########################################

print("-" * 80)
print("Clustering mit Matrizen")


cluster_adj1 = ["groß", "klein", "kurz", "lang", "schwarz", "rot", "weiß"]
cluster_adj2 = cluster_adj1 + ["grün"]


print(adjectives.cluster(adjectives=cluster_adj1, num_clusters=6,
                         by_centroid=True))
print()
print(adjectives.cluster(adjectives=cluster_adj2, num_clusters=6,
                         by_centroid=True))




print("-" * 80)
########################################
########################################






#####################################
######### CLUSTER MIT NOMEN #########
#####################################

print("-" * 80)
print("Clustering mit Nomen")


cluster_adj3 = ["neu", "alt", "groß", "klein", "kurz", "lang",
                "schwarz", "rot", "gleich", "verschieden"]
cluster_nouns = ["Welt", "Fuß", "Kopf", "Leute", "Stimme", "Sache",
                 "Gedanke", "Haus", "Vater"]


for noun in cluster_nouns:
    print("Cluster bzgl. des Nomens", noun)
    print(adjectives.cluster_with_nouns(adjectives=cluster_adj3, noun=noun,
                                        num_clusters=6, by_centroid=True))


    print()



print("-" * 80)
########################################
########################################








######################################
####### MATRIX POTENZIEREN NAH #######
######################################

print("-" * 80)
print("Gemittelte Matrix-Potenz-Entfernungen")

power_adj = ["neu", "alt", "groß", "klein", "gleich", "verschieden", "schwarz", "rot"]

power_nouns = ['Leben', 'Weise', 'Arbeit', 'Licht', 'Frau', 'Stelle', 'Blick', 'Hand', 'Dame', 'Land', 'Herr', 'Jahr', 'Auge', 'Mensch', 'Wasser', 'Teil', 'Platz', 'Kind', 'Herz', 'Leute', 'Bewegung', 'Welt', 'Mädchen', 'Vater', 'Lage', 'Augenblick', 'Ding', 'Sache', 'Zug', 'Mann', 'Raum', 'Schritt', 'Name']

adjectives.get_mean_distances_by_power(power_adj, power_nouns, powers=[1,2,3,4,5])


print("-" * 80)
######################################
######################################







###################################################
###### ERZEUGUNG UND ANORDNUNG AUF DER SKALA ######
###################################################
print("-" * 80)
print("Skalen erzeugen.")

### verwendete Skalen in Dokumentation ###
scales = [
    (["letzter", "erster", "zweiter"], ["Tag", "Monat", "Jahr"]),
    (["lang", "kurz"], ["Stunde", "Minute", "Sekunde", "Jahr", "Monat", "Tag"]),
    (["rot", "schwarz", "weiß"], ["Blut", "Gesicht", "Herz"]),
    (["groß", "klein", "kurz", "lang"], ["Zug", "Platz", "Stadt"]),
    (["gleich", "verschieden"], ["Sache", "Ding", "Teil"]),
    (["alt", "neu"], ["Sache", "Ding", "Teil"])
]

for adjs, nns in scales:
    print("Adjektive:", adjs, "  Nomen:", nns)
    print(adjectives.arrange_on_spectrum(adjs, nns))




### andere, nicht-verwendete Skalen ###
other_scales = [
    (["stark", "schwach"], ["Mann", "Frau", "Kind", "Mädchen", "Junge"]),
    (["deutsch", "englisch"], ["Mann", "Frau", "Kind", "Mädchen", "Junge"]),
]


for adjs, nns in other_scales:
    print("Adjektive:", adjs, "  Nomen:", nns)
    print(adjectives.arrange_on_spectrum(adjs, nns))


print("-" * 80)
###################################################
###################################################









#######################################################
###### BEDEUTUNGSÄHNLICHKEIT MIT MATRIXINVERSION ######
#######################################################

print("-" * 80)
print("Bedeutungsähnlichkeit mit Matrixinversion")

meaning_sim_nouns = ["Blick", "Leben", "Mädchen", "Weise", "Frau", "Wasser", "Hand", "Herz", "Welt", "Name", "Herr", "Auge"]


for adj1 in all_adj:
    print("-" * 80)
    print("Untersuche Adjektiv {}:".format(adj1))
    print("-" * 80)

    for noun in meaning_sim_nouns:
        valid_adj = []

        for i, adj2 in enumerate(all_adj):
            close_ones = adjectives.there_and_back(adj1, adj2, 
                                                   noun,
                                                   power=1,
                                                   by_centroid=True)

            if noun in [n for _, n in close_ones]:
                valid_adj.append(adj2)

        print("Ähnliche Adjektive bezüglich des Nomens {}: {}".format(noun, valid_adj))
        print("-+-" * 35)



print("-" * 80)
#######################################################
#######################################################







##########################
###### DETERMINANTE ######
##########################
print("-" * 80)
print("Determinante")

for adj in all_adj:
    print(adj, "hat Determinante", adjectives.get_det(adj, by_centroid=True))


print("-" * 80)
##########################
##########################
