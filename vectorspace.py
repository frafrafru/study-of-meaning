

class Vectorspace():
    """Abstrake Vektorraum-Klasse, die Vektorrepräsentationen
    trägt und gewisse Operationen mit diesen Vektoren ermöglicht."""


    import filereader
    import numpy as np
    from os import getcwd


    def __init__(self):
        self.base_path = self.getcwd()

        # setze grundlegende Attribute eines Vektorraumes
        for att in ["vectors", "id2word", "word2id"]:
            setattr(self, att, None)
        
        # initialisiere eine Ähnlichkeitsfunktion
        self.similarity_func = self._cos_sim
        self.highest_best = True

        # Lade alle zum Vektorraum gehörenden Dateien.
        self.load()


    def words2vecs(self, words):
        """Gibt die Vektorrepräsentation eines Wortes bzw. die
        Vektorrepräsentationen von Wörtern bei Angabe einer Liste
        von Wörtern zurück.
        
        :param list/str words: Ein String oder eine Liste von Strings
                               der/des Worte(s), deren/dessen Vektor(en) 
                               zurückgegeben werden soll(en).
            
        :return: Liste (Vektor) oder Liste von Listen (Liste von Vektoren)
                 der/des angegebenen Worte(s)
        :rtype: list"""

        if isinstance(words, list):
            return [self.vectors[vec_id] for vec_id in self.words2ids(words)]
        elif isinstance(words, str):
            return self.vectors[self.words2ids(words)]
        elif isinstance(words, tuple) and self.__class__() == AdjectiveNounVectorspace:
            return self.vectors[self.words2ids(words)]
        else:
            raise TypeError("words must be either a list of words or a single word")

    def ids2words(self, ids):
        """Gibt das zu der ID gehörende Wort eines Integers zurück.
        Wird eine Liste von Integern angegeben, so werden die zu 
        den Integern gehörenden Wörter als Liste zurückgegeben.
        
        :param list/int words: Ein Integer oder eine Liste von Integern
                               (IDs), dessen/deren Worte/Wörter
                               zurückgegeben werden soll(en).
            
        :return: String oder Liste von String der angegebenen ID(s)
        :rtype: list/str"""

        # Überprüfe, ob es eine Liste oder ein Integer ist.
        if isinstance(ids, list):
            return [self.id2word[w_id] for w_id in ids]
        elif isinstance(ids, int):
            return self.id2word[ids]
        else:
            raise TypeError("ids must be either a list of ids or a single id")


    def words2ids(self, words):
        """Gibt die eindeutige ID bei Angabe eines String zurück,
        die zu diesem Wort gehört. Wird eine Liste von Strings
        angegeben, so werden die zu den Wörtern gehörenden IDs als
        Liste zurückgegeben.
        
        :param list/str words: Ein String oder eine Liste von Strings
                               der/des Worte(s), deren/dessen IDs 
                               zurückgegeben werden soll(en).
            
        :return: ID oder Liste von IDs der/des angegebenen Worte(s)
        :rtype: list/int"""
        

        # Überprüfe, ob es eine Liste oder ein String ist.
        if isinstance(words, list):
            return [self.word2id[word] for word in words]
        elif isinstance(words, str):
            return self.word2id[words]
        else:
            raise TypeError("words must be either a list of words or a single word")


    def get_all_words(self):
        return [b for _, b in self.id2word.items()]


    def set_similarity_function(self, similarity_fnc="norm"):
        """Setzt klassenintern die zu verwendende Ähnlichkeits-
        funktion. Mögliche Wahl: 'cos', 'cosine', 'dist', o.Ä.
        
        :param str similarity_fnc: Name der zu verwendenen Ähnlich-
                                   keitsfunktion."""

        if similarity_fnc.lower() in ["cos", "cosine"]:
            self.similarity_func = self._cos_sim
            self.highest_best = True
        elif similarity_fnc.lower() in ["dist", "euclidean", "norm"]:
            self.similarity_func = self._dist
            self.highest_best = False
        else:
            raise NotImplementedError("The specified similarity function does not exist.")


    def get_closest_to_word(self, word, num_closest=10, sim_func=None):
        """Gibt das Wort zurück, welches am nächsten an dem Vektor
        des angegebenen Wortes liegt.
        
        :param str word: Wort, dessen nächste Nachbarn bestimmt 
                         werden sollen.
                       
        :param int num_closest: Anzahl der nächsten Wörter, die
                                zurückgegeben werden sollen.
                                
        :param str sim_func: Kann z.B. 'cos' oder 'dist' sein.
                             Gibt die Metrik an, die verwendet
                             wird, um den Abstand zu berechnen.
                             
        :return: gibt eine Liste an nächsten Wörtern zurück.
        :rtype: list"""

        if sim_func:
            self.set_similarity_function(sim_func)

        word_vec = self.vectors[self.word2id(word)]
        return self.get_closest_to_vector(word_vec, num_closest, sim_func)


    def get_closest_to_vector(self, vec, num_closest=10, sim_func=None):
        """Gibt das Wort zurück, welches am nächsten an dem angegebenen
        Vektor liegt.
        
        :param list vec: Vektor, dessen nächste Nachbarn bestimmt werden
                         sollen.
                       
        :param int num_closest: Anzahl der nächsten Wörter, die
                                zurückgegeben werden sollen.
                                
        :param str sim_func: Kann z.B. 'cos' oder 'dist' sein.
                             Gibt die Metrik an, die verwendet
                             wird, um den Abstand zu berechnen.
                             
        :return: gibt eine Liste an nächsten Wörtern zurück.
        :rtype: list"""

        if sim_func:
            self.set_similarity_function(sim_func)

        vec_distances = []
        for other_vec_id, other_vec in enumerate(self.vectors):
            sim = self.similarity_func(other_vec, vec)
            vec_distances.append((other_vec_id, sim))
        
        # sort vectors by its distances
        closest = sorted(vec_distances, key=lambda x:x[1], reverse=self.highest_best)
        closest_vecs = self.ids2words([vec_id for vec_id, _ in closest[1:num_closest+1]])

        # only return second to number of wanted vectors + 1
        # because first will be word_vec itself
        return closest_vecs


    def get_closest_to_id(self, id, num_closest=10, sim_func=None):
        """Gibt das Wort zurück, welches am nächsten an dem Vektor
        mit der angegebenen ID liegt.
        
        :param int id: eindeutige ID des Wortes, dessen nächste
                       Nachbarn bestimmt werden sollen.
                       
        :param int num_closest: Anzahl der nächsten Wörter, die
                                zurückgegeben werden sollen.
                                
        :param str sim_func: Kann z.B. 'cos' oder 'dist' sein.
                             Gibt die Metrik an, die verwendet
                             wird, um den Abstand zu berechnen.
                             
        :return: gibt eine Liste an nächsten Wörtern zurück.
        :rtype: list"""

        return self.get_closest_to_word(word=self.id2word(id),
                                        num_closest=num_closest,
                                        sim_func=sim_func)


    # Ähnlichkeitsfunktionen
    def _cos_sim(self, x, y):
        return self.np.dot(x, y)/(self.np.linalg.norm(x) * self.np.linalg.norm(y))
    def _dist(self, x, y):
        return self.np.linalg.norm(x-y)


    # müssen in Unterklassen implementiert werden
    def load(self):
        raise NotImplementedError("Subclass must implement train method.")

    def train(self):
        raise NotImplementedError("Subclass must implement train method.")
            



class AdjectiveNounVectorspace(Vectorspace):

    def load(self):
        """Lädt Vektorrepräsentationen in den Arbeitsspeicher."""

        # Versuche Dateien zu laden. Falls sie nicht existieren, werden
        # diese zunächst erstellt.
        try:
            vecs, wmap = self.filereader.load_vs(self.base_path + "/an_vs")

            self.vectors = vecs
            self.id2word = dict([(a, tuple(b)) for a, b in wmap.items()])
            self.word2id = dict([(tuple(b), a) for a, b in wmap.items()])

        except FileNotFoundError:
            # Der Vektorraum existiert nicht und muss zuerst erstellt werden
            self.train()


    def train(self, dimension=300):
        """Ruft build_an_vs auf, um Vektorrepräsentationen
        zu erzeugen.
        
        :param int dimension: die gewünschte Dimension der
                              Vektorrepräsentationen."""

        from buildvs import build_an_vs, preprocess_in_parallel
        build_an_vs(preprocess_in_parallel(), dimension)

    
    def get_ans_by_adj(self, adj):
        """Gibt eine Liste von ANs mit gegebenem Adjektiv
        zurück.
        
        :param str adj: das Adjektiv, welches in allen ANs
                        auftauchen soll.
                         
        :return: Liste von ANs mit gegebenem Adjektiv.
        :rtype: list"""
        return [an for an in self.get_all_words() if adj == an[0]]

    
    def get_adjectives_from_ans(self):
        """Gibt alle Adjektive zurück, die im Vektorraum
        in ANs auftreten.
        
        :return: Liste aller existierenden Adjektive.
        :rtype: list"""

        return list(set([a for a, _ in self.get_all_words()]))


    def get_an_mean_by_noun(self, noun):
        """Gibt einen von ANs gemittelten Vektor
        zu einen gegebenem Nomen zurück (Zentroid).
        
        :param str noun: das Nomen für das der Zentroid 
                         berechnet werden soll.
                         
        :return: zentroider Vektor
        :rtype: list"""

        # enthält eine Liste der Form [("grün", "Baum"), ("alt", "Baum"), ...]
        all_noun_ans = [a for a in self.get_all_words() if a[1] == noun]    

        if len(all_noun_ans) == 0:
            return None
            # print("No ANs found.")
            # return  [0] * 300

        # ANs in Vektoren umwandeln
        all_an_vecs = self.words2vecs(all_noun_ans)

        # über alle Einträge mitteln
        return [self.np.mean(*a) for a in zip(self.np.transpose(all_an_vecs))]





class NounVectorspace(Vectorspace):
    
    def load(self):
        """Lädt Vektorrepräsentationen in den Arbeitsspeicher."""

        # Versuche Dateien zu laden. Falls sie nicht existieren, werden
        # diese zunächst erstellt.
        try:
            vecs, wmap = self.filereader.load_vs(self.base_path + "/nn_vs")

            self.vectors = vecs
            self.id2word = wmap
            self.word2id = dict([(b, a) for a, b in wmap.items()])
        except FileNotFoundError:
            # the vector space must be created first
            self.train()

    def train(self, dimension=300):
        """Ruft build_vs auf, um Vektorrepräsentationen
        zu erzeugen.
        
        :param int dimension: die gewünschte Dimension der
                              Vektorrepräsentationen."""

        from buildvs import build_vs, preprocess_in_parallel
        build_vs(preprocess_in_parallel, dimension)
