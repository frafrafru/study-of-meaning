import nltk
nltk.download("punkt")
nltk.download("stopwords")


# contains german tagger and lemmatizer
from HanTa import HanoverTagger as ht
tagger = ht.HanoverTagger('morphmodel_ger.pgz')


from filereader import get_raw_text_bounds


def preprocess(indexraw_text):
    """Funktion, die von einem Prozess in preprocess_in_parallel
    aufgerufen wird. Tokenisiert, lemmatisiert und entfernt
    Stoppwörter.
    
    :param tuple indexraw_text: Tupel der Form (Index, str)
                                wobebi Index der Index des 
                                gestarteten Prozesses ist.
                                
    :return: Liste mit tokenisierten Sätzen
    :rtype: list"""

    index, raw_text = indexraw_text
    print("Neuer Prozess gestartet (" + str(index) + ")")

    lang = "german"
    remove_stopwords = True

    if remove_stopwords:
        stopwords = set(nltk.corpus.stopwords.words(lang))
        stopwords.union(set(["ja", "nein", "ich", "du", "er", "sie", "es", "wir", "ihr"]))

    # Text tokenisieren
    sents = nltk.sent_tokenize(raw_text, language=lang)
    tokenized_sents = [nltk.word_tokenize(sent, language=lang) for sent in sents]

    # annotate (pos tags) and remove punctuation
    # produce data format of [
    #   [("LEMMA", "POS")]      # sentence 1
    #   [("LEMMA", "POS")]      # sentence 2
    #   ...
    # ]

    processed_sents = []

    # Hier werden Stoppwörter und ähnliches entfernt.
    for tokenized_sent in tokenized_sents:
        if remove_stopwords:
            proc_sent = [(lemma, pos) for _, lemma, pos in tagger.tag_sent(tokenized_sent) 
                                      if not lemma == "--" and not lemma.lower() in stopwords]
        else:
            proc_sent = [(lemma, pos) for _, lemma, pos in tagger.tag_sent(tokenized_sent) 
                                      if not lemma == "--"]
        processed_sents.append(proc_sent)
    
    print("Prozess abgeschlossen ({})".format(index))
    return processed_sents



def preprocess_in_parallel(batch_size=15, num_proc=10):
    """Tokenisiert den vorgegebenen Text, welcher in
    dem Ordner src abgespeichert ist. Hierbei kommt 
    Multiprocessing zur Anwendung.
    
    :param int batch_size: Größe der Batches.
    
    :param int num_proc: Anzahl verwendeter Prozesse.
    
    :return: Liste, die tokenisierte Sätze in Form von
             Listen enthält.
    :rtype: list"""


    import multiprocessing as mp


    # Die Anzahl der Textdokumente im Ordner src
    MAX_FILES = 300

    print("Erstelle Batches mit Größe", batch_size)

    # Erstelle die Grenzen für die Batches
    bounds = [(i * batch_size, (i+1) * batch_size) for i in range(int(MAX_FILES/batch_size))]
    corpus_batches = {i: get_raw_text_bounds(*bound) for i, bound in enumerate(bounds)}

    print("Tokenisierung beginnt (parallel mit {} Prozessen)".format(num_proc))

    with mp.Pool(processes=num_proc) as pool:
        results = pool.map(preprocess, corpus_batches.items())

    # die von mp.Pool zurückgegebenen Ergebnisse zusammenführen
    # und zurückgeben.
    return [sent for sents in results for sent in sents]