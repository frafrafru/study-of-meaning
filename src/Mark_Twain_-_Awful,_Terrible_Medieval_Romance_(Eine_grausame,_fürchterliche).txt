Mark Twain

Eine grausame, fürchterliche Liebesgeschichte aus dem Mittelalter

Übersetzt von Winfried Heppner © 2005

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Das Geheimnis wird gelüftet

Es war Nacht. Stille herrschte über dem ehrwürdigen alten Familienschloss derer
von Klugenstein. Das Jahr 1222 neigte sich seinem Ende entgegen. Hoch oben im
höchsten Turm des Schlosses flackerte ein einsames Licht. Eine geheime
Besprechung fand dort statt. Der gestrenge alte Baron von Klugenstein saß auf
einem prächtigen Stuhl und dachte nach. Plötzlich sagte er in einem milden Ton:

»Meine Tochter!«

Ein junger Mann von edler Erscheinung, von Kopf bis Fuß in eine Ritterrüstung
gekleidet, antwortete:

»Sprecht, mein Vater!«

»Meine Tochter, die Zeit ist gekommen dir das Geheimnis zu eröffnen, das dich
dein ganzes junges Leben lang beschäftigt hat. So wisse denn, dass es seinen
Ursprung in den Dingen hat, die ich dir nun enthüllen werde. Mein Bruder Ulrich
ist der Herzog von Brandenburg. Unser Vater hatte auf seinem Totenbett verfügt,
dass, sollte Ulrich keinen Sohn haben, die Erbfolge auf unsere Familie
übergeht, sollte ich einen Sohn haben. Und weiter, falls keiner einen Sohn,
sondern nur Töchter haben sollte, die Erbfolge auf Ulrichs Tochter übergeht,
falls sie unbefleckt bleibt; sollte dies nicht der Fall sein, folge meine
Tochter unter der Bedingung nach, dass sie sich einen tadellosen Namen bewahrt.
Uns so kam es, dass ich und meine alte Frau hier inbrünstig dafür beteten, mit
einem Sohn gesegnet zu werden. Unsere Gebete aber wurden nicht erhört. Ich war
verzweifelt. Ich musste zusehen, wie mir der große Preis aus den Händen zu
gleiten drohte, der wunderbare Traum zerplatzte. Und ich hatte doch solch große
Hoffnungen gehegt! Fünf Jahre war Ulrich nun verheiratet, aber seine Frau hatte
keinen Erben, sei es Mädchen oder Junge, geboren.

»Aber halt«, sagte ich, »noch ist nicht alles verloren.« Ein rettender Trick
war mir ein-gefallen. Du wurdest um Mitternacht geboren. Nur der Hofschranze,
die Kinderfrau und sechs Dienerinnen kannten dein Geschlecht. Es war keine
Stunde vergangen, da hatte ich sie schon alle aufgehängt. Am nächsten Morgen
spielte die ganze Baronie verrückt, weil verkündet worden war, dass Klugenstein
ein Sohn und dem mächtigen Brandenburg ein Erbe geboren war! Und das Geheimnis
wurde all die Zeit gewahrt. Die Schwester deiner Mutter kümmerte sich in deiner
Kindheit um dich und von dieser Zeit an hatten wir keine Angst mehr.

Als du zehn Jahre alt warst, wurde Ulrich eine Tochter geboren. Wir waren sehr
traurig, hofften aber auf das wohltätige Werk der Masern, der Ärzte oder
anderer natürlicher Feinde der Kindheit, doch wir wurden immer wieder
enttäuscht. Sie lebte, sie gedieh – alle Plagen des Himmels über sie! Aber das
hat nichts zu bedeuten. Wir sind sicher. Denn – ha, ha – haben wir keinen Sohn?
Und ist nicht unser Sohn der künftige Herzog? Unser geliebter Conrad, ist es
nicht so? – denn, Frau von 28 Jahren, die du bist, mein Kind, kein anderer Name
als dieser ist dir je gegeben worden.

Nun hat es sich so gefügt, dass das Alter seine Hand auf meinen Bruder gelegt
hat und er immer schwächer wird. Die Sorgen um den Staat machen ihm das Leben
schwer. Deshalb wünscht er, dass du zu ihm kommst und schon jetzt Herzog wirst
– de facto, wenn auch nicht de iure. Unsere Gefolgsleute sind bereit – du
trittst deine Reise noch heute Nacht an.

Und nun höre mir gut zu. Denke an jedes Wort, das ich sage. Es gibt ein Gesetz,
das so alt ist wie Deutschland. Es besagt, dass eine Frau, die auch nur einen
Moment auf dem großen herzoglichen Thron sitzt, bevor sie in Gegenwart des
Volkes rechtsgültig gekrönt worden ist, STERBEN MUSS. Höre also auf meine
Worte! Gib dich demütig. Verkünde deine Entscheidungen vom Stuhl des
Premierministers, der am Fuße des Thrones steht. Tue das, bis du gekrönt und
sicher bist. Es ist unwahrscheinlich, dass dein Geschlecht je entdeckt wird;
aber es zeugt von Weisheit, wenn man alles so gut wie möglich absichert in
dieser Welt voller Verräter.«

»Oh mein Vater, ist aus diesem Grunde mein Leben eine Lüge gewesen? Sollte ich
meine unschuldige Cousine ihrer Rechte berauben? Verschont mich, Vater,
verschont euer Kind!«

»Was soll der Quatsch? Ist das der Dank für das erhabene Glück, das mein Gehirn
für sich geschaffen hat? Bei den Gebeinen meines Vaters, deine winselnde
Gefühlsduselei schlägt sich mir auf die Galle.

Begib dich zum Herzog, sofort! Und überlege dir gut, wie du mit meinen
Absichten verfährst.«

Genug von diesem Gespräch. Es genügt uns zu wissen, dass die Gebete, das Flehen
und die Tränen des sanftmütigen Mädchens nichts fruchteten. Weder sie noch
irgendetwas anderes konnten den mannhaften Herrn von Klugenstein bewegen. Und
so sah die Tochter schließlich schweren Herzens, wie sich die Tore der Burg
hinter ihr schlossen und musste begleitet von einer ritterlichen Schar
bewaffneter Vasallen und einer stattlichen Anzahl treuer Diener in die
Dunkelheit reiten.

Nach der Abreise seiner Tochter saß der alte Baron minutenlang schweigend da,
dann wandte er sich an seine traurige Frau und sagte:

»Herrin, mit unseren Anliegen scheint es nun schnell voranzugehen. Es ist jetzt
genau drei Monate her, dass ich den verschlagenen und gut aussehenden Grafen
Detzin Constanze, meines Bruders Tochter geschickt habe, um seinen teuflischen
Auftrag auszuführen. Wenn er versagt, sind wir nicht vollständig in Sicherheit,
aber wenn er Erfolg hat, dann kann keine Macht der Welt unsere Tochter davon
abhalten, Herzogin zu werden, auch wenn eine unglückliche Entwicklung dafür
sorgen sollte, dass sie nie Herzog wird.«»Mein Herz ist voller Ahnungen, doch
alles kann zu einem guten Ende kommen.«

»Ach was, Frau! Lass das Unken den Kröten. Zu Bett mit dir, und träume von
Brandenburg und Hoheit!«

Festlichkeiten und Tränen

Sechs Tagen nach den Ereignissen, über die wir im vorigen Kapitel berichtet
haben, erstrahlte die glänzende Hauptstadt des Herzogtums Brandenburg von
militärischem Pomp und überall erschallte der Jubel der getreuen Menge; Conrad,
der junge Thronerbe war angekommen. Das Herz des alten Herzogs war voller
Glückseligkeit, denn Conrads wohlgestalte Person und sein huldvolles Betragen
hatte ihn sofort für sich entflammt. In den großen Sälen des Palastes drängten
sich die Adeligen, die Conrad ehrerbietig willkommen hießen. Und alles schien
so hell und fröhlich, dass er spürte, wie sein Kummer sich auflöste und einer
tröstlichen Zufriedenheit wich.

In einer abgelegenen Kammer des Palastes spielte sich jedoch eine ganz andere
Szene ab. An einem Fenster stand das einzige Kind des Herzogs, die junge Herrin
Constanze. Ihre Augen waren rot geschwollen und voller Tränen. Gleich fing sie
wieder zu weinen an und sagte laut:

»Der Verbrecher Detzin ist weg – er ist aus dem Herzogtum geflohen. Ich konnte
das zunächst nicht glauben, aber ach, es ist nur zu wahr. Und ich habe ihn doch
so geliebt. Ich habe es gewagt, ihn zu lieben, obwohl ich doch wusste, dass
mein Vater, der Herzog, mir nie erlauben würde, ihn zu heiraten. Ich habe ihn
geliebt – jetzt aber hasse ich ihn! Von ganzer Seele hasse ich ihn. Oh, was ist
aus mir geworden! Ich bin verloren, verloren, verloren. Ich werde noch
verrückt!«

Die Lage spitzt sich zu

Einige Monate gingen ins Land. Jedermann sang das hohe Lied der Regierungskunst
des jungen Conrad, betonte die Weisheit seiner Entscheidungen, die
Barmherzigkeit in seinen Urteilen und die Bescheidenheit mit der er sich in
seinem bedeutenden Amt trug. Der alte Herzog legte bald alles in seine Hände,
saß abseits und hörte mit stolzer Befriedigung zu, wie dieser die Verordnungen
der Krone vom Stuhl des Premierministers verkündete. Es schien klar, dass
einer, der von jedermann so geliebt, gelobt und geehrt wurde wie Conrad,
einfach glücklich sein musste. Aber merkwürdigerweise war er das ganz und gar
nicht. Denn er hatte voller Widerwillen bemerkt, dass sich die Prinzessin
Constanze in ihn verliebt hatte. Die Liebe aller anderen war ein Glücksfall für
ihn, aber diese hier barg erhebliche Gefahren in sich! Und er hatte außerdem
bemerkt, dass dem Herzog die Leidenschaft seiner Tochter ebenfalls nicht
entgangen war und er schon von einer Hochzeit träumte. Tag für Tag verblasste
ein bisschen von der tiefen Trauer, die auf dem Gesicht der Prinzessin gelegen
hatte; Tag für Tag strahlten Hoffnung und wiedergewonnenes Leben heller aus
ihren Augen; und nach und nach huschte sogar ein Lächeln über das Gesicht, das
so bekümmert gewesen war.

Conrad fand das entsetzlich. Er verfluchtet sich bitterlich dafür, dass er dem
Instinkt nachgegeben hatte, die Gesellschaft einer Person seines eigenen
Geschlechts gesucht zu haben, als er noch neu und fremd im Palast gewesen war –
als er voller Kummer war und sich nach der Art von Mitleid sehnte, die nur eine
Frau nachvollziehen kann und zu geben versteht. Er fing an, seiner Cousine aus
dem Weg zu gehen. Das machte die Sache aber nur noch schlimmer, denn, je mehr
er sie mied, desto mehr drängte sich natürlich zu ihm hin. Zuerst wunderte er
sich darüber; dann verblüffte es ihn. Dieses Mädchen verfolgte ihn; sie jagte
hinter ihm her; sie lief ihm ständig und überall über den Weg, Tag und Nacht.
Sie schien eigenartig angespannt. Da gab es irgendwo ein Geheimnis.

Das konnte einfach nicht so weitergehen. Die ganze Welt sprach davon. Der
Herzog wurde allmählich verwirrt. Aus dem armen Conrad wurde als Folge seiner
Angst und seiner äußersten Not ein regelrechter Geist. Eines Tages, als er aus
einem Vorzimmer der Bildergalerie heraustrat, stellte ihn Constanze, packte ihn
an beiden Händen und rief aus:

»Oh, warum meidest du mich? Was habe ich getan – was habe ich gesagt, dass ich
deine freundliche Meinung von mir verloren habe – denn gewiss hatte ich sie ja
einst. Conrad, weise mich nicht zurück, sondern habe Erbarmen mit einem
gequälten Herzen. Ich kann die Worte nicht länger zurückhalten, ohne dass sie
mich töten – ICH LIEBE DICH, CONRAD! Weise mich zurück, wenn du musst, aber ich
musste sie aussprechen.«

Conrad verschlug es die Sprache. Constanze wartete einen Augenblick, und dann –
weil sie sein Schweigen falsch interpretierte –dann glühte in ihren Augen
wildes Entzücken auf und sie schlang ihre Arme um seinen Hals und sagte:

»Du lässt dich erweichen, du lässt dich erweichen! Du kannst mich lieben, du
wirst mich lieben! Oh, sag, dass du mich lieben wirst, mein einziger, mein
angebeteter Conrad!«

Conrad seufzte laut. Eine kranke Blässe überlief sein Antlitz und er zitterte
wie Espenlaub. Im selben Augenblick stieß er voller Verzweiflung das Arme
Mädchen von sich und schrie:

»Du weißt nicht, was du da verlangst! Es ist ein für alle Mal unmöglich! Und
dann ergriff er wie ein Verbrecher die Flucht und ließ die Prinzessin völlig
verdattert zu-rück. Eine Minute später lag sie da, weinte und seufzte dort,
Conrad weinte und seufzte in seinem Zimmer. Beide waren verzweifelt. Beide
hatten ihren Untergang vor Augen.

Nach und nach kam Constanze wieder auf die Füße und entfernte sich. Sie sagte:

»Der bloße Gedanke, dass er meine Liebe genau in dem Moment zurückgewiesen hat,
als ich glaubte, die bringe sein grausames Herz zum Schmelzen! Ich hasse ihn!
Er hat mich weggestoßen – ja – weggestoßen wie einen Hund!«

Die furchtbare Enthüllung

Die Zeit verging. Eine gefasste Traurigkeit ruhte noch einmal auf dem Antlitz
der Tochter des guten Herzogs. Sie und Conrad bekam niemand mehr zusammen zu
Gesicht. Der Herzog war darüber betrübt. Aber im Laufe der Wochen kam wieder
Farbe in Conrads Wangen, seine alte Lebhaftigkeit kehrte in sein Auge zurück
und er versah die Regierungsgeschäfte mit klarem und ständig reifer werdendem
Verstand.

Da hob allmählich ein merkwürdiges Flüstern im Palast an. Es wurde lauter; es
griff um sich. Die Klatschmäuler der Stadt bekamen Wind davon. Und das wurde
geflüstert:

»Prinzessin Constanze hat ein Kind zur Welt gebracht!«

Als Herr von Klugenstein davon hörte, schlang er seinen Helmbusch dreimal um
seinen Kopf und rief:

»Lang lebe Herzog Conrad! – denn siehe, seine Krone ist gesichert vom heutigen
Tage an! Detzin hat seinen Auftrag erfüllt und der gute Gauner soll seinen Lohn
bekommen!«

Und er verkündete die frohe Nachricht in nah und fern und achtundvierzig
Stunden land gab es in der ganzen Baronie keine Seele, die nicht getanzt oder
gesungen, gefeiert oder ein Freudenfeuer entzündet hätte, um dieses großartige
Ereignis zu feiern; und alles auf Kosten des stolzen und glücklichen alten
Klugenstein.

Die schreckliche Katastrophe

Der Prozess rückte näher. All großen Herren und Barone Brandenburgs waren im
Gerichtssaal des herzoglichen Palastes versammelt. Kein Platz blieb unbesetzt,
an dem ein Beobachter sitzen oder stehen konnte. Conrad, gekleidet in Purpur
und Hermelin, saß im Stühle des Premierministers und zu beiden Seiten hatten
die höchsten Richter des Reiches Platz genommen. Der alte Herzog hat strengste
Anweisung gegeben, dass der Prozess gegen seine Tochter unvoreingenommen zu
führen sei und sich dann mit gebrochenem Herzen in sein Bett begeben. Seine
Tage waren gezählt. Der arme Conrad hatte wie um sein Leben darum gebettelt,
dass man es ihm das Elend ersparen möge, über das Verbrechen seiner Cousine zu
Gericht sitzen zu müssen, aber es hatte nichts genutzt.

Das traurigste Herz in dieser ganzen großen Versammlung schlug in Conrads
Brust.

Das freudigste schlug in der seines Vaters. Denn, ohne dass seine Tochter
»Conrad« etwas davon gewusst hätte, hatte sich der alte Baron Klugenstein
eingefunden und hielt sich unter all den Adeligen auf – voll des Triumphs zum
Wohle seines Hauses.

Nachdem die Herolde den Beginn der Veranstaltung ausgerufen hatten und die
an-deren Präliminarien vorüber waren, sagte der ehrenwerte Oberste Richter:

»Gefangene, tretet vor!«

Die unglückliche Prinzessin erhob sich und stand ohne Schleier vor der
unübersehbaren Menge. Der Oberste Richter fuhr fort:

»Meine hochwohlgeborene Dame, vor den Hohen Gerichtshof dieses Reiches ist
Anklage erhoben und bewiesen worden, dass Ihr Euer Gnaden außerhalb des
ehelichen Bundes eines Kindes entbunden worden seid; und nach unserem
altehrwürdigen Gesetz steht darauf die Todesstrafe, mit einer einzigen
Ausnahme, von der Euch seine Gnaden, der amtierende Herzog, der gute Herr
Conrad, nun in seinem feierlichen Urteil in Kenntnis setzen wird; so gebt denn
acht!

Conrad streckte zögerlich sein Zepter nach vorne, und in demselben Moment
begann das weibliche Herz unter seiner Robe voller Mitleid gegenüber der
verdammten Gefangenen zu klagen, und Tränen traten ihm in die Augen. Er öffnete
seine Lippen, um zu sprechen, aber der Oberste Richter ergriff das Wort:

»Nicht hier, mein Gebieter, nicht hier! Es ist wider das Gesetz ein Urteil
gegen ein Mitglied der herzoglichen Familie von einem anderen Platz zu
verkünden, ALS VOM HERZOGLICHEN THRON!«

Ein Schaudern lief durch das Herz des armen Conrad, und ein Zittern durchfuhr
auch die eiserne Gestalt seines Vaters. CONRAD WAR NOCH NICHT GEKRÖNT – sollte
er es wagen, den Thron zu entweihen? Er zögerte und wurde blass vor Angst. Aber
es musste sein. Aller Augen richteten sich schon auf ihn. Man würde Verdacht
schöpfen, wenn er noch länger zögerte. Er bestieg den Thron. Da streckte er
sein Zepter erneut aus und sagte:

»Gefangene, im Namen unseres Gebieters, des Herrn Ulrich, Herzog von
Branden-burg, erfülle ich die ernste Pflicht, die mir aufgetragen ist. Höre
meine Worte. Nach dem altehrwürdigen Gesetz unseres Landes musst du sterben,
wenn du nicht den Teilhaber an deiner Schuld vorführst und ihn dem Henker
auslieferst. Ergreife diese Gelegenheit – rette dich, so lange du noch kannst.
Nenne uns den Vater deines Kindes!«

Ein ernstes Schweigen fiel über den großen Gerichtshof – ein Schweigen, das so
tief war, dass die Männer ihren eigenen Herzschlag hören konnten. Dann drehte
sich die Prinzessin langsam um, ihre Augen glühten vor Hass und indem sie mit
dem Finger geradewegs auf Conrad deutete, sagte sie:

»Du bist der Mann!«

Eine widerliche Überzeugung seiner eigenen hilflosen, hoffnungslosen Gefährdung
ließ Conrads Herz so kalt werden, als ob es schon tot sei. Welche Macht der
Erde könnte ihn denn retten! Um die Anklage zu entkräften, musste er enthüllen,
dass er eine Frau war; und als ungekrönte Frau auf dem herzoglichen Thron zu
sitzen bedeutete den Tod! In ein und demselben Moment verloren er und sein
verbitterter alte Vater das Bewusstsein und fielen zu Boden.

[Den Rest dieser spannenden und ereignisreichen Geschichte wird man WEDER in
dieser NOCH in einer anderen Veröffentlichung finden, weder jetzt noch
irgendwann in der Zukunft.]

Um die Wahrheit zu sagen, ich habe meinen Helden [bzw. meine Heldin] dermaßen
in die Bredouille gebracht, dass ich nicht erkennen kann, wie ich ihn [bzw.
sie] sie da je wieder herausbringen soll. Aus diesem Grunde lasse ich jetzt die
Finder davon und überlasse es der betreffenden Person, möglichst heil aus der
Sache herauszukommen - oder ansonsten drin zu bleiben. Ich hatte gedacht, es
würde ziemlich einfach sein, die kleine Schwierigkeit zu beheben, aber das
sieht nun gar nicht danach aus.

[Wenn Harper's Weekly oder die New York Tribune den Wunsch hegen sollten, die
vorliegenden Eingangskapitel in ihren geschätzten Zeitschriften zu
veröffentlichen, so wie sie das mit den Eingangskapiteln von Romanen bei Ledger
und New York Weekly machen, so steht ihnen dies frei – zu den üblichen
Konditionen, vorausgesetzt, sie »glauben mir«.]

