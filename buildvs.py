# from preprocess import preproc
# import filereader as fr
from preprocess import preprocess_in_parallel


import numpy as np
import scipy.sparse as sparse
from sklearn.decomposition import TruncatedSVD

import multiprocessing as mp

import json, time, gc, math


def comp_coocc_an(an_id, an, length_cooc, bigrams_flat, rev_map):
    """Funktion zur Berechnung eines Kookkurrenz-Vektors zu
    gegebenem AN. Wird verwendet um parallel Kookkurrenzen
    zu berechnen.
        
    :param int an_id: Eindeutige ID des ANs, wie es im Vokabular
                      gewählt wurde.

    :param tuple an: Tupel das Strings als Einträge der Form
                     ("grün", "Haus"), etc. enthält.

    :param int length_cooc: Die Länge des Vektors die zurückgegeben
                            wird. Wird nur benötigt, da es sonst
                            immer neu berechnet werden würde.

    :param list bigrams_flat: Liste den gesamten Korpus enthält.
                              Flat steht hier dafür, dass diese Lis-
                              te keine weiteren Listen enthält, son-
                              dern nur Bigramme.

    :param dict rev_map: Wörterbuch, dass dazu verwendet wird per
                         Bigramm auf seine ID zurückzugreifen. Hat
                         die Form {("grün", "Haus"): 0, ...}.
                              
    :return: Tupel der Form (an_id, Vektor mit Kookkurrenzen).
    :rtype: tuple"""

    
    # Kontextgröße
    window_size = 12

    co_occ_vector = [0] * length_cooc

    # Durch den gesamten Korpus iterieren um Kookkurrenzen mit dem
    # festen AN zu zählen
    for num, ((lemma1, _), (lemma2, _)) in enumerate(bigrams_flat):

        # Überprüfe an welchen Stellen das angegebene AN auftritt
        if an == (lemma1, lemma2):

            # Kontext wählen
            lower_b = max(0, num - window_size)
            upper_b = min(len(bigrams_flat), num + window_size + 1)
            context = bigrams_flat[lower_b: upper_b]
        
            # Durch den Kontext iterieren, um Kookkurrenzen mit
            # anderen ANs, die im Vokabular liegen, zu zählen
            for (lemma1, _), (lemma2, _) in context:
                if (lemma1, lemma2) in rev_map:
                    co_occ_vector[rev_map[(lemma1, lemma2)]] += 1
                
    # gib an_id zurück, damit nach Aufruf dieser Methode der Vektor
    # mit den Kookkurrenzen zugeordnet werden kann
    # (wird benötigt, weil mehrere parallel laufen)
    return an_id, co_occ_vector



def comp_coocc_batch(an_ids, ans, length_cooc, bigrams_flat, rev_map):
    """Funktion zur Berechnung mehrerer Kookkurrenz-Vektoren zu
    gegebener AN-Batch. Diese Funktion wird von mp.Pool aufgerufen.
        
    :param list an_ids: Liste eindeutiger IDs der ANs, wie sie im 
                        Vokabular gewählt wurden.

    :param list ans: Liste von Tupeln, die jeweils Strings als Einträge
                     der Form ("grün", "Haus"), etc. enthalten.

    :param int length_cooc: Die Länge der Vektoren die zurückgegeben
                            werdn. Wird nur benötigt, da es sonst
                            immer neu berechnet werden würde.

    :param list bigrams_flat: Liste den gesamten Korpus enthält.
                              Flat steht hier dafür, dass diese Lis-
                              te keine weiteren Listen enthält, son-
                              dern nur Bigramme.

    :param dict rev_map: Wörterbuch, dass dazu verwendet wird per
                         Bigramm auf seine ID zurückzugreifen. Hat
                         die Form {("grün", "Haus"): 0, ...}.
                              
    :return: Liste der Form [(an_id1, Vektor mit Kookkurrenzen), ...].
    :rtype: list"""


    print("Started ids: [{}, ..., {}]".format(min(an_ids), max(an_ids)))

    cooc_vectors = []

    # Berechne für jedes AN in der Batch die Kookkurrenzen
    for an_id, an in zip(an_ids, ans):
        cooc_vectors.append(comp_coocc_an(an_id, an, length_cooc, 
                                          bigrams_flat, rev_map))
    
    print("Finished ids: [{}, ..., {}]".format(min(an_ids), max(an_ids)))

    return cooc_vectors


def build_an_vs(input_text, output_dim, max_words=80000, num_proc=10):
    """Erstellt Vektorrepräsentationen der angegebenen Dimension
    output_dim für die häufigsten max_words ANs zu gegebenem
    Korpus. Speichert diesen zusammen mit dem Vokabular-Wörterbuch
    im angegebenen Verzeichnis im Order an_vs ab.
        
    :param str input_text: Nach Sätzen tokenisierter Korpus der
                           Form [[("Es", "NE"), ("sein", "VVFIN"), 
                                  ...
                                  ],
                                  [("Roter", "ADJA"), ("Haus", "NN"),
                                  ...
                                  ]
                                ].

    :param int output_dim: Dimension der Vektorrepräsentationen.

    :param int max_words: Die Anzahl der häufigsten ANs, die be-
                          rücksichtigt werden sollen.
                          
    :param int num_proc: Anzahl benutzter Prozesse bei Kookkurrenz-
                         zählung. Man bemerke, dass bei hoher Zahl
                         auch viel RAM benötigt wird."""


    # hier werden eindeutige ANs abgespeichert, um das Vokabular
    # zu erzeugen
    unique_relevant_an = set()

    print("Extrahiere eindeutige ANs...")
    # Iteriere über alle tokenisierten Sätze
    for tokenized_sent in input_text:

        # Iteriere über jedes Bigramm im Satz
        for i, (lemma, pos) in enumerate(tokenized_sent):

            # Überprüfe, ob Bigramm der gewünschten Form ist,
            # also ob es ein AN ist.
            if pos in ["ADJA"]:
                try:
                    lemma_n, pos_n = tokenized_sent[i+1]
                    if pos_n == "NN":
                        unique_relevant_an.add((lemma, lemma_n))

                except IndexError:
                    pass    # ADJA am Satzende



    # speichere hier den gesamten Korpus in einer
    # kontinuierlichen Liste ohne verschachtelte Listen ab.
    all_bigrams_flat = []

    for sentence in input_text:
        for i in range(len(sentence)-1):
            all_bigrams_flat.append((sentence[i], sentence[i+1]))


    # Berechne Häufigkeiten der einzelnen ANs, um später
    # die max_words häufigsten zu behalten.
    print("Berechne Häufigkeiten der ANs.")
    counter = dict([(an, 0) for an in unique_relevant_an])
    for (lemma1, pos1), (lemma2, pos2) in all_bigrams_flat:

        # Nur ANs werden gezählt.
        if pos1 in ["ADJA"] and pos2 in ["NN"]:
            counter[(lemma1, lemma2)] += 1

    # Sortiere nach Häufigkeiten
    sorted_cnt = sorted(counter.items(), reverse=True, key= lambda x:x[1])

    # Schneide alles hinter max_words ab und behalte den Teil davor
    counter = dict(sorted_cnt[:max_words])

    # Diese Liste enthält die eindeutigen Bigramme, ohne ihre
    # Häufigkeiten
    unique_relevant_an = [bigram for bigram, _ in counter.items()]


    # Erstelle ein Wörterbuch, welches jedem AN eine eindeutige
    # ID zuweist. Wird benötigt, damit Vektoren später wieder-
    # identifizierbar sind.

    # 1. Wörterbuch der Form {1: "Haus", 2: "Mensch", ...}
    relev_an_map = dict(enumerate(unique_relevant_an))

    # 2. Wörterbuch der Form {"Haus": 1, "Mensch": 2, ...} 
    relev_an_map_rev = dict([(b, a) for a, b in relev_an_map.items()])








    # Ab hier beginnt der Prozess der Kookkurrenzberechnung.
    # Da dies sehr lange dauert, werden mehrere Prozesse benutzt

    print("Zählen der Kookkurrenzen... Dies kann viel Zeit in Anspruch nehmen.")

    
    # Initialisiere die Variable, die im Verlauf als scipy.sparse Matrix
    # die Kookkurrenzen speichert. Würde man keine Sparse-Matrix
    # benutzen, so würden Arbeitsspeicher-Probleme auftreten.
    co_occ_sparse = None


    # Diese Variable nur verändern, falls das Programm bei der Berechnung
    # aufgrund eines vollen Arbeitsspeichers o.Ä. unterbrochen wurde.
    START_AT = 0

    # Fertige Indizes werden abgespeichert, damit man eine prozentuale
    # Übersicht während der Berechnung anzeigen kann.
    finished_indices = list(range(START_AT))

    # Hier wird die insgesamt bei der Berechnung verstrichene Zeit
    # in Sekunden hochgezählt.
    total_time = 0


    # Anzahl der ANs, die pro Prozess berechnet werden. Hier gilt:
    # je mehr ANS, desto mehr RAM wird verwendet.
    batch_size_per_call = 200

    # Hier werden die Argumente drin gespeichert, die für den mp.Pool-
    # Aufruf übergeben werden.
    args = []
    temp_an_ids, temp_ans = [], []


    # Iteriere über alle max_words ANs
    for an_id, an in list(relev_an_map.items())[START_AT:]:

        # ANs und an_ids werden gesammelt, damit
        # sie an mp.Pool übergeben werden können
        temp_an_ids.append(an_id)
        temp_ans.append(an)

        # Wenn eine Batch-Größe erreicht wurde, dann wird ein Übergabeargument
        # für mp.Pool erstellt und in args gespeichert. Die Überprüfung
        # an_id == (max_words - 1) ist nötig für den Fall, dass max_words kein
        # Vielfaches von batch_size_call ist. Sonst blieben ganz am Ende die
        # letzten ANs übrig.
        condition = ((an_id % batch_size_per_call == 0 and not an_id == START_AT) 
                     or an_id == (max_words - 1))
        if condition:
            args.append((temp_an_ids, temp_ans, len(relev_an_map),
                         all_bigrams_flat.copy(), relev_an_map_rev.copy()))

            # abgearbeitete an_ids werden in die entsprechende Liste gehängt.
            # Die Listen, die Argumente sammeln werden zurückgesetzt.
            finished_indices += temp_an_ids
            temp_an_ids, temp_ans = [], []


        # Wenn genügend Argumente gesammelt wurden, d.h. so viele
        # wie es Prozesse gibt
        condition = ((an_id % (batch_size_per_call * num_proc) == 0 
                      and not an_id == START_AT)
                     or (max_words - 1) in finished_indices)
        if condition:

            print("Aufruf von mp.Pool wird gestartet...")

            # Um verstrichene Zeit zu messen
            t0 = time.time()

            # übergebe gesammelte Argumente an mp.Pool.
            # Hier findet der Multicore-Teil statt.
            with mp.Pool(processes=num_proc) as pool:
                res = pool.starmap(comp_coocc_batch, args)

            # Sortiere Ergebnisse nach an_id
            flat_res = [(an_id, cooc_vec) for temp_res in res 
                                          for an_id, cooc_vec in temp_res]
            sorted_res = sorted(flat_res, key=lambda x:x[0])

            # erster Fall tritt ein, wenn es der erste Durchlauf ist.
            # Falls der zweite Fall eintritt, so kann man die Sparse-
            # Matrizen einfach übereinander stapeln.
            if co_occ_sparse == None:
                mat = [cooc_vec for _, cooc_vec in sorted_res]
                co_occ_sparse = sparse.csr_matrix(mat)
            else:

                stack_mat = [cooc_vec for _, cooc_vec in sorted_res]
                co_occ_sparse = sparse.vstack((co_occ_sparse, stack_mat))


            args, res, sorted_res = [], [], []
            print(gc.collect(), "Objekte aufgeräumt, die nicht mehr benötigt werden.")


            # print out progress message
            percent = round(len(finished_indices)/len(relev_an_map.items()) * 100, 2)
            seconds = round((time.time()-t0), 2)
            total_time += seconds


            printout_str = "Finished pooling ({} minutes). Total: {} minutes. Progress: {}%"
            print(printout_str.format(round(seconds/60, 2), round(total_time/60, 2), percent))
            print()




    # sparse.savez("coocc_mat.npz", co_occ_sparse)
    


    print("Kookkurrenzzählen abgeschlossen.")
    print("Assoziierungswerte werden berechnet...")

    # Assoziierungswerte berechnen
    assoc_mat = _turn_into_assoc_scores_sparse(co_occ_sparse, counter, relev_an_map)

    # Dimensionsreduktion
    vs = _reduce_dim(assoc_mat, output_dim)


    # Speichern
    print("Speichere Vektoren...")
    np.savetxt("an_vs/vs.csv", vs, delimiter=",")

    with open('nn_vs/wordmap.json', 'w', encoding='utf-8') as f:
        json.dump(relev_an_map, f, ensure_ascii=False, indent=4)
 

    print("Fertig!")








def build_vs(input_text, output_dim, max_words=10000):
    """Erstellt Vektorrepräsentationen der angegebenen Dimension
    output_dim für die häufigsten max_words NNs zu gegebenem
    Korpus. Speichert diesen zusammen mit dem Vokabular-Wörterbuch
    im angegebenen Verzeichnis im Order nn_vs ab.
        
    :param str input_text: Nach Sätzen tokenisierter Korpus der
                           Form [[("Es", "NE"), ("sein", "VVFIN"), 
                                  ...
                                  ],
                                  [("Roter", "ADJA"), ("Haus", "NN"),
                                  ...
                                  ]
                                ].

    :param int output_dim: Dimension der Vektorrepräsentationen.

    :param int max_words: Die Anzahl der häufigsten NNs, die be-
                          rücksichtigt werden sollen."""
                          

    # Kontextdefinition. Zahl hinten gibt die Größe an
    co_occ_method = "window_12"  

    # Nur NN Vektoren sollen erstellt werden (möglich auch noch NE)
    relevant_pos = ["NN"]

    # Eindeutige NNs zählen
    unique_relevant_words = set()

    # Iteriere über alle Sätze und Wörter um eindeutige
    # NNs zu identifizieren.
    for tokenized_sent in input_text:
        for lemma, pos in tokenized_sent:
            if pos in relevant_pos:
                unique_relevant_words.add(lemma)


    # Liste des Korpus ohne verschachtelte Listen
    all_sents_flat = [word for sentence in input_text for word in sentence]


    # Häufigkeiten der NNs zählen
    counter = dict([(word, 0) for word in unique_relevant_words])
    for lemma, pos in all_sents_flat:
        if pos in relevant_pos:
            counter[lemma] += 1
    
    # Nach Häufigkeiten sortieren, um nur max_words zu behalten
    sorted_cnt = sorted(counter.items(), reverse=True, key= lambda x:x[1])
    counter = dict(sorted_cnt[:max_words])

    # Menge aller max_words häufigsten Wörter
    unique_relevant_words = set([word for word, _ in counter.items()])

    # Wörterbücher werden erstellt, um eine Zuordnung 
    # NN -> ID und umgekehrt zu erhalten

    # 1. hat die Form {1: "Haus", 2: "Mensch", ...}
    relev_word_map = dict(enumerate(unique_relevant_words))

    # 2. hat die Form {"Haus": 1, "Mensch": 2, ...}
    relev_word_map_rev = dict([(b, a) for a, b in relev_word_map.items()])




    # Hier werden alle Kookkurrenzen in einer Matrix gespeichert.
    co_occ_mat = []


    # Iteriere über alle NNs
    for relev_word_id, relev_word in relev_word_map.items():

        co_occ_vector = [0] * len(relev_word_map)

        if "window" in co_occ_method:
            window_size = int(co_occ_method.split("_")[1])

            # dort wo ein bestimmtes NN auftritt, erstelle Kontext
            # und zähle Kookk. aller anderen NNs die dort auftreten
            for num, (word, _) in enumerate(all_sents_flat):
                if relev_word == word:
                    lower_b = max(0, num - window_size)
                    upper_b = min(len(all_sents_flat), num + window_size + 1)
                    context = all_sents_flat[lower_b: upper_b]
                
                    for word, pos in context:
                        if pos in relevant_pos:
                            co_occ_vector[relev_word_map_rev[relev_word]] += 1


        # Diese Art zu zählen könnte man noch implementieren,
        # war hier aber nicht relevant.
        elif co_occ_method == "sentence":
            raise NotImplementedError

        # Hänge Vektor an die Matrix an.
        co_occ_mat.append(co_occ_vector)


    print("Kookkurrenzzählung abgeschlossen. Starte Assoziierungswertberechnung")

    # Assoziierungswerte berechnen
    assoc_mat = _turn_into_assoc_scores(co_occ_mat, counter, relev_word_map)

    # Dimensionsreduktion
    vs = _reduce_dim(assoc_mat, output_dim)

    print("Speichern...")
    np.savetxt("nn_vs/vs.csv", vs, delimiter=",")

    with open('nn/wordmap.json', 'w', encoding='utf-8') as f:
        json.dump(relev_word_map, f, ensure_ascii=False, indent=4)

    print("Fertig!")



def _turn_into_assoc_scores(mat, occurrences, word_map):
    """Verwandelt eine Matrix mat gemäßg Pointwise Mutual
    Information in eine andere Matrix und gibt diese zurück.
        
    :param list mat: Liste, die eine Form einer symmetrischen
                     Matrix ist.
    
    :param dict occurrences: Wörterbuch, die Häufigkeiten für
                             jedes Wort enthält.

    :param dict word_map: Wörterbuch für Zuweisung der NNs/ANs
                          zu ihren eindeutigen IDs.

    :return: np.array mit den transformierten Einträgen gem. PMI.      
    :rtype np.array:"""


    pmi_mat = []
    total_num_cooccurences = sum([cell for i, row in enumerate(mat) for cell in row[i:]])
    total_num_occurences = sum([occ for _, occ in occurrences.items()])

    for num_row, row in enumerate(mat):
        px = occurrences[word_map[num_row]] / total_num_occurences

        pmi_row = []
        for num_cell, y in enumerate(row):
            py = occurrences[word_map[num_cell]] / total_num_occurences
            pxy = y / total_num_cooccurences
            if pxy == 0:
                pmi_row.append(0)
            else:
                pmi_row.append(math.log(pxy / (px*py), 2))

        pmi_mat.append(pmi_row)

    return np.array(pmi_mat)


def _turn_into_assoc_scores_sparse(mat, occurences, word_map):
    """Genauso wie _turn_into_assoc_scores jedoch für
    scipy.sparse.csrmatrix. Der Rückgabewert ist hier ebenfalls
    eine scipy.sparse-Matrix."""

    total_num_cooccurences = sum([mat[i,j] for i,j in zip(*mat.nonzero()) if i <= j])
    total_num_occurences = sum([occ for _, occ in occurences.items()])

    pmi_mat = sparse.csr_matrix(mat.shape)

    for i,j in zip(*mat.nonzero()):
        px = occurences[word_map[i]] / total_num_occurences
        py = occurences[word_map[j]] / total_num_occurences

        pxy = mat[i,j] / total_num_cooccurences

        pmi_mat[i,j] = math.log(pxy / (px * py), 2)

    return pmi_mat



def _reduce_dim(coocc_mat, output_dim):
    """Reduziert die Dimension einer (Kookkurrenz-)Matrix
    auf die angegebene Dimension.
    
    :param list coocc_mat: Liste in der Form einer Matrix.
    
    :param int output_dim: Dimension auf die, die angegebene
                           Matrix reduziert werden soll.
                           
    :return: reduzierte Matrix
    :rtype: np.array"""

    np.random.seed(4618)
    coocc_mat_reduced = None
    
    svd = TruncatedSVD(n_components = output_dim, n_iter = 10)
    coocc_mat_reduced = svd.fit_transform(coocc_mat)

    return coocc_mat_reduced




prepr_text = preprocess_in_parallel()


print("erstelle Vektordarstellungen NN")
build_vs(prepr_text, 300)

print("erstelle Vektordarstellungen AN")
build_an_vs(prepr_text, 300, 80000)


