import json, csv
from os import listdir, getcwd



ADJECTIVE_PATH = getcwd() + "/adjectives"
TXT_PATH = getcwd() + "/src"


def get_raw_text():
    """Liest aus dem oben angegebenen Pfad Textdateien ein
    und wandelt diese in einen String um, welcher zurück-
    gegeben wird.
    
    :return: aus dem Pfad eingelesene Textdateien
    :rtype: str"""

    input_text = ""

    for fn in listdir(TXT_PATH):
        with open(TXT_PATH + "/" + fn) as f:
            input_text += f.read()

    if input_text == "":
        raise FileNotFoundError("No text files in {} were found for training".format(TXT_PATH))

    return input_text


def get_raw_text_bounds(frm=0, to=100):
    """Ähnlich wie get_raw_text werden in dieser Funktion
    Textdateien eingelesen. Die Grenzen geben an, von welcher
    bis zu welcher Datei eingelesen werden soll.
    
    :param int frm: untere Grenze
    
    :param int to: obere Grenze (ausschließend)
    
    :return: eingelesenen Text innerhalb der angegebenen
             Grenzen.
    :rtype: str"""

    input_text = ""

    for fn in listdir(TXT_PATH)[frm: to]:
        with open(TXT_PATH + "/" + fn) as f:
            input_text += f.read()

    if input_text == "":
        raise FileNotFoundError("No text files in {} were found for training".format(TXT_PATH))

    return input_text



def load_vs(path):
    """Liest einen Vektorraum-Pfad. In diesem Pfad
    müssen die Dateien vs.csv und wordmap.json enthalten
    sein.
    
    :param str path: Pfad zum zu ladenen Vektorraum
    
    :return: Tupel bestehend aus einer Liste mit
             den eingelesenen Vektoren und ein 
             Wörterbuch, welches die ID-Zuordnungen
             enthält.
    :rtype: tuple"""
    
    vectors = []
    try:
        with open(path + "/vs.csv", newline='') as vs_file:
            vs_reader = csv.reader(vs_file, delimiter=",")
            vectors = [[float(entry) for entry in row] for row in vs_reader]
    except:
        raise FileNotFoundError("The vector space file does not exist.")    

    wordmap = dict()
    try:
        with open(path + "/wordmap.json") as wm_file:
            wm_data = json.load(wm_file)

            # turn numbers into actual numbers (they are stored as strings)
            wordmap = dict([(int(a), b) for a, b in wm_data.items()])
    except:
        raise FileNotFoundError("The wordmap file does not exist.")

    return vectors, wordmap


def load_adjective(adjective, centroid=False):
    """Liest eine .csv-Datei und gibt sie anschließend als
    Liste in Form einer Matrix zurück.
    
    :param str adjective: Name des Adjektivs, z.B. 'rot'
    
    :param bool centroid: Wenn True, wird die durch Zentroide
                          erstellte Matrix zurückgegeben.
                          
    :return: Gibt eine Liste in Form einer Matrix zurück
    :rtype: list"""

    adj = []

    if not centroid:
        load_path = ADJECTIVE_PATH + "/{}.csv".format(adjective)
    else:
        load_path = ADJECTIVE_PATH + "/{}_c.csv".format(adjective)
    
    with open(load_path, newline='') as adj_file:
        adj_reader = csv.reader(adj_file, delimiter=",")
        adj = [[float(entry) for entry in row] for row in adj_reader]

    return adj